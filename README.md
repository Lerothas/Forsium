<!--
SPDX-FileCopyrightText: 2022 Stefan W. 'Lerothas' Draxlbauer <lerothas@mailbox.org>

SPDX-License-Identifier: CC-BY-NC-SA-4.0
-->

# Forsium

Ein kleines Programm geschrieben in Rust und ~~GTK4~~ Iced, um im Unterricht zufällig Schüler:innen für Abfragen auszuwählen und Noten dazu zu notieren.

## Roadmap

### Version 1.0: Ducatus

- [x] CLI-Interface
- [x] Klasse wählen
- [x] Schüler wird zufällig gewählt
- [x] Note in Tabelle eintragen
- [x] bei Abwesenheit geht ein Zähler nach oben
- [x] Abwesenheiten werden gelistet
- [x] Nachdem eine Note erteilt wurde, geht der Zähler nach unten
- [x] Wahrscheinlichkeit abhängig von Zählerstand, Abwesenheit und zuletzt erteilten Noten.
- [ ] ~~Datenbank verschlüsselt. Entweder als Datei verschlüsseln oder die Datenbank selbst verschlüsseln.~~ Wird nicht implementiert. **Verschlüsselung bleibt den Nutzenden überlassen.**  
(siehe unten für Empfehlungen)
- [x] Funktionen in Module aufgeteilt
- [x] cli-Interface schreiben
- [ ] Klasse aus CSV-Datei einlesen
- [ ] Daten exportieren als CSV-Datei
- [x] Grafisches Interface mit ~~GTK 4~~ Iced
- [x] Einstellungen werden gespeichert (Pfad zur Datenbank)
- [x] Abwesenheit per GUI eintragen
- [x] Abwesenheit wird protokolliert
- [x] Noten bearbeiten
- [x] Abwesenheiten löschen

## Kompilieren

Voraussetzungen: Es werden die folgenden Programme gebraucht, um Forsium zu kompilieren:

* rustc
* cargo
* rustup

1. Klonen
2. Crates über Cargo updaten:

> cargo update

3. Programm übersetzen:

> cargo build

4. Programm ausführen:

> cargo run

## Verschlüsselung

Forsium speichert Daten in einer sqlite-Datenbank mit der Standard-Endung .forsium zur Unterscheidung. Das ist eine Datei wie viele andere und kann dementsprechend bspw. in verschlüsselten Containern mittels

- VeraCrypt
- zuluCrypt
- Vaults/Plasma Vaults

und anderen verschlüsselt werden.

Siehe dazu auch [Video-Reihe zur Verschlüsselung von Schüler:innen-Daten](https://video-cave-v2.de/w/p/tQgAVtub3YfwXNKA8FrVpZ).

## Benutzung

![Bildschirmfoto_Stand_2023-07-25](Bildschirmfoto_Stand_2023-07-25.png)
