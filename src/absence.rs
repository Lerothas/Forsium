// SPDX-FileCopyrightText: 2022 Stefan W. 'Lerothas' Draxlbauer <lerothas@mailbox.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::current_date_as_int::current_date_as_int;

#[derive(Clone,Debug)]
pub struct Absence{
    pub id: u32,
    pub student_id: u32,
    pub surname: String,
    pub forename: String,
    pub course: String,
    pub date: u32,
}
impl Default for Absence{
    fn default() -> Absence{
        Absence {
            id: 0,
            student_id: 0,
            surname: String::from(""),
            forename: String::from(""),
            course: String::from(""),
            date: current_date_as_int(),
        }
    }
}

#[allow(dead_code)]
impl Absence{
    pub fn name( &self ) -> String{
        let vor = &self.forename;
        let nach = &self.surname;
        format!( "{vor} {nach}" )//, self.vorname, self.nachname )
    }

    pub fn date( &self ) -> String {
        let s = self.date.to_string();
        let v: Vec<&str> = s.split("").collect();
        // println!("{:?} -> {:?} -> {:?}", self.date, s, v);
        let string =
            String::from( v[7] )
            + v[8]
            + "."
            + v[5]
            + v[6]
            + "."
            + v[1]
            + v[2]
            + v[3]
            + v[4]
            ;
        return string.clone();
    }
}
impl std::fmt::Display for Absence {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!( f, "{} {}, {}, am {}", &self.forename, &self.surname, &self.course, &self.date() )
    }
}
