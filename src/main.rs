// SPDX-FileCopyrightText: 2022 Stefan W. 'Lerothas' Draxlbauer <lerothas@mailbox.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

mod database;
// mod cli;
mod student;
mod grade;
mod gradescale;
mod absence;
mod window;
mod current_date_as_int;

// static APP_NAME: String = String::from( "Forsium" );

fn main() {

    // cli::start_cli();
    let _ = window::run_gui();
}
