// use crate::student::Student;

use std::fs::File;
// use std::io::{self, BufRead};
// use std::path::Path;
use std::error::Error;
// use std::process;
use crate::student::Student as Student;
use crate::gradescale::GradeType as GradeType;
use crate::database::Database as Database;

// SQLite
// use rusqlite::{Connection, Result};

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
// #[allow(dead_code)]
// fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
// where P: AsRef<Path>, {
//     let file = File::open(filename)?;
//     Ok(io::BufReader::new(file).lines())
// }

#[allow(dead_code)]
pub fn import_csv( path_to_file: &String, database: &Database )
 -> Result<(), Box<dyn Error>>
{
    let mut counter = 0;
    // Open Database:

    // Open file:
    let file = File::open( path_to_file )?;
    // Read whole csv file in rdr: StringRecord
    let mut rdr = csv::ReaderBuilder::new()
        .delimiter(b',')
        .from_reader( file );

    let headers = rdr.headers().unwrap();
    println!( "Header: {:?} mit der Länge {}", headers, headers.len() );
    for i in headers.iter(){
        // Einfügen: Prüfen, welches der Stichwörter i entspricht => Speichern als Zahl für Zugriff später
    }
    // Iterate over all records and insert them into database
    for result in rdr.records() {
        let record = result?;
        println!("Gelesener Record: {:?}", record);    // Debug Output of whole StringRecord
        // let ergebnis = database.student_new(
        // // let student =
        // &Student{
        //         id: 0,
        //         forename: record[1].to_string(),
        //         surname: record[0].to_string(),
        //         counter: 2,
        //         course: record[2].to_string(),
        //         grade_type: GradeType::from_string( record[6].to_string() ),
        //         comment: record[5].to_string(),
        //         place_number: record[7].to_string().parse::<u32>().unwrap(),
        //     }
        // // ;
        // );
        // // println!( "{:?}", student );
        // println!( "{:?}", ergebnis );
        counter += 1;
    }

    println!( "Found {} entries.", counter );

    Ok(())

}

// Function that checks if a class and a subject exists in the definite
// table in the sql database
// fn database_check_if_class_and_subject_exist( conn: &Connection, class: &String, subject: &String )
// -> bool {
//     let mut stmt = conn.prepare(
//         "SELECT * FROM 'Kurse' WHERE ( Kurse=:kurs);",
//     )?;
//     let classes = stmt.query_map(&[(":kurs", class)], |row| {
//         Ok()
//     }
//
//
// }

// pub fn insert_into_sqlite( conn: &Connection, nachname: &String, vorname: &String, kurs: &String, class: &String, fach: &String, comment: &String )
// -> Result<()>
// {
//
//     conn.execute(
//         "INSERT INTO students (surname, forename, course, class, subject, comment) VALUES (?1, ?2, ?3, ?4, ?5, ?6);",
//         ( &nachname, &vorname, &kurs, &class, &fach, &comment),
//     )?;
//
//     Ok(())
// }
