// SPDX-FileCopyrightText: 2022 Stefan W. 'Lerothas' Draxlbauer <lerothas@mailbox.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use rusqlite::{Connection, Result};//, NO_PARAMS};

#[allow(dead_code)]
pub fn create( database_path: &str ) -> Result<()>{
	println!( "Erstelle Datenbank: {}", database_path );
	// Check if file path is writable

	// create sqlite3 file and open it
	let conn = Connection::open( database_path )?;
	// create tables:

	// // create table list-of-classes
	// conn.execute("
    //     CREATE TABLE 'list-of-classes' (
	// 		'ID'	INTEGER NOT NULL UNIQUE,
	// 		'class'	TEXT NOT NULL UNIQUE,
	// 		PRIMARY KEY('ID' AUTOINCREMENT)
	// 	);",
	// 	[],
    // )?;
	//
	// // create table list-of-subjects
	// conn.execute("
	// 	CREATE TABLE 'list-of-subjects' (
	// 		'ID'	INTEGER NOT NULL UNIQUE,
	// 		'subject'	TEXT NOT NULL UNIQUE,
	// 		PRIMARY KEY('ID' AUTOINCREMENT)
	// 	);",
	// 	[],
	// )?;
	//
	// // create table classes-and-topics
	// let result = conn.execute("
	// 	CREATE TABLE 'classes-and-subjects' (
	// 		'ID'	INTEGER NOT NULL UNIQUE,
	// 		'class'	TEXT NOT NULL,
	// 		'subject'	TEXT NOT NULL,
	// 		FOREIGN KEY('subject') REFERENCES 'list-of-subjects'('subject') ON DELETE RESTRICT,
	// 		FOREIGN KEY('class') REFERENCES 'list-of-classes'('class') ON DELETE RESTRICT,
	// 		PRIMARY KEY('ID' AUTOINCREMENT)
	// 		);",
	// 	[],
	// )?;
	// println!( "Fehler: {}", result);

	// create table classes-and-topics
	let result = conn.execute("
		CREATE TABLE 'Kurse' (
			'ID'	INTEGER NOT NULL UNIQUE,
			'Kursname'	TEXT NOT NULL UNIQUE,
			'Klasse'	TEXT,
			'Fach'	TEXT,
			PRIMARY KEY('ID' AUTOINCREMENT)
		);",
		[],
	)?;
	println!( "Fehler bei Kurse: {}", result);

	// create table students
	let result = conn.execute("
		CREATE TABLE 'students' (
			'ID'		INTEGER NOT NULL UNIQUE,
			'course' 	TEXT NOT NULL,
			'class'		TEXT,
			'subject'	TEXT,
			'surname'	TEXT NOT NULL,
			'forename'	TEXT NOT NULL,
			'comment'	TEXT,
			'counter'	INTEGER DEFAULT 2,
			'placenumber'	INTEGER,
			PRIMARY KEY('ID' AUTOINCREMENT)
		);",

		// FOREIGN KEY('course') REFERENCES 'Kurse'('Kursname'),
		// FOREIGN KEY('subject') REFERENCES 'Kurse'('Fach'),
		// FOREIGN KEY('class') REFERENCES 'Kurse'('Klasse'),
		[],
	)?;
	println!( "Fehler bei students: {}", result);

	// create table absences
	let result = conn.execute("
		CREATE TABLE 'absences' (
			'ID'	INTEGER NOT NULL UNIQUE,
			'studentID'	INTEGER NOT NULL,
			'course' 	TEXT NOT NULL,
			'date'	INTEGER NOT NULL,
			FOREIGN KEY('studentID') REFERENCES 'students'('ID'),
			PRIMARY KEY('ID' AUTOINCREMENT)
		);",
		[],
	)?;
	println!( "Fehler bei absences: {}", result);

	// create table grades
	let result = conn.execute("
		CREATE TABLE 'grades' (
			'ID'	INTEGER NOT NULL UNIQUE,
			'date'	INTEGER NOT NULL,
			'grade'	REAL NOT NULL,
			'grade_type'	TEXT,
			'course'	TEXT,
			'studentID'	INTEGER NOT NULL,
			'Notiz'	TEXT,
			FOREIGN KEY('studentID') REFERENCES 'students'('ID') ON DELETE RESTRICT,
			PRIMARY KEY('ID' AUTOINCREMENT)
		);",
		[],
	)?;
	println!( "Fehler bei grades: {}", result);

	Ok(())
}

#[allow(dead_code)]
pub fn create_dummy_database() -> (String, bool){
	let result = create( "dummy-database.db" );
	match result {
		Err(e) => (format!("{:?}", e), false),
		Ok(v) => ( format!("{:?}", v), true),
	}

	// fill with dummy data
}
