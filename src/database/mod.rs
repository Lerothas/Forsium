// SPDX-FileCopyrightText: 2022 Stefan W. 'Lerothas' Draxlbauer <lerothas@mailbox.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::student::Student;
use crate::grade::Grade;
use crate::gradescale::GradeType;
use crate::absence::Absence;
// SQLite
use rusqlite::{Connection, Result};
// use std::path::Path;

use rand::Rng;
// use std::path::Path;

pub mod create_database;
pub mod importcsv;

#[derive(Debug, Clone)]
struct Table{
    name: String,
}

#[allow(dead_code)]
pub struct Database {
    database_path: String,
    conn: Connection,//Option<Connection>,
}

#[allow(dead_code)]
#[allow(unused_mut)]
impl Database {
    // pub fn new() -> Self {
    //     Self{
    //         database_path: String::from(""),
    //         conn: None,
    //     }
    // }

    pub fn open_connection( path: &String ) -> Self{
        // let db =
        Self{
            database_path: path.clone(),
            conn: Connection::open( path ).unwrap(),
        }//;
        // match db.check_database(){
        //     Err( e ) => println!( "Fehler: {:?}", e),
        //     Ok( _ ) => (),
        // };
        // return db;
    }

    pub fn return_path( &self ) -> String {
        self.database_path.clone()
    }

    // pub fn create( &self ) -> Result<()>{
    //     match self.table_create_absences(){
    //         Err( e ) =>
    //         Ok(_) =>
    //     }
    // }

    fn table_create_courses( &self ) -> Result<()>{
        let result = self.conn.execute("
            CREATE TABLE 'Kurse' (
                'ID'	INTEGER NOT NULL UNIQUE,
                'Kursname'	TEXT NOT NULL UNIQUE,
                'Klasse'	TEXT,
                'Fach'	TEXT,
                PRIMARY KEY('ID' AUTOINCREMENT)
            );",
            [],
        )?;
        println!( "Fehler bei Kurse: {:?}", result);
        Ok(())
    }

    fn table_create_students( &self ) -> Result<()>{
        let result = self.conn.execute("
            CREATE TABLE 'students' (
                'ID'		INTEGER NOT NULL UNIQUE,
                'course' 	TEXT NOT NULL,
                'surname'	TEXT NOT NULL,
                'forename'	TEXT NOT NULL,
                'comment'	TEXT NOT NULL,
                'counter'	INTEGER DEFAULT 2,
                'gradetype' TEXT NOT NULL,
                'placenumber'	INTEGER NOT NULL,
                PRIMARY KEY('ID' AUTOINCREMENT)
            );",
            [],
        )?;
        println!( "Fehler bei students: {:?}", result);
        Ok(())
    }

    fn table_create_absences( &self ) -> Result<()>{
        let result = self.conn.execute("
            CREATE TABLE 'absences' (
                'ID'	INTEGER NOT NULL UNIQUE,
                'studentID'	INTEGER NOT NULL,
                'course' 	TEXT NOT NULL,
                'date'	INTEGER NOT NULL,
                PRIMARY KEY('ID' AUTOINCREMENT)
            );",
            [],
        )?;
        println!( "Fehler bei absences: {:?}", result);
        Ok(())
    }

    fn table_create_grades( &self ) -> Result<()>{
        let result = self.conn.execute("
            CREATE TABLE 'grades' (
                'ID'	INTEGER NOT NULL UNIQUE,
                'date'	INTEGER NOT NULL,
                'grade'	REAL NOT NULL,
                'gradetype'	TEXT NOT NULL,
                'course'	TEXT NOT NULL,
                'studentID'	INTEGER NOT NULL,
                'Notiz'	TEXT,
                PRIMARY KEY('ID' AUTOINCREMENT)
            );",
            [],
        )?;
        println!( "Fehler bei grades: {:?}", result );
        Ok(())
    }

    pub fn check_database( &self ) -> Result<()>{
        // String für Fehler:
        let mut errors = String::from("");
        // Tables mit diesen Namen müssen vorhanden sein:
        // let vec = vec![ "Kurse", "students", "absences", "grades"];
        // Prüfung per SQLite Abfrage:
        let mut stmt = self.conn.prepare(
            "SELECT name FROM sqlite_master;",
        )?;
        let tables = stmt.query_map(
            [],
            |row| {
                Ok(
                    Table{
                        name: row.get( "name" )?,
                    }
                )
            }
        )?;

        let mut vec_tables: Vec<String> = Vec::new();
        for table in tables{
            vec_tables.push( String::from( table.unwrap().name.clone() ) );
        }
        // vec_tables.dedup();

        if !vec_tables.contains( &String::from( "Kurse" ) ){
            match self.table_create_courses(){
                Err(e) => {
                    println!( "Fehler:{:?}\n", e );
                    errors.push_str( format!( "Fehler:{:?}\n", e ).as_str() );
                },
                Ok(_) => (),
            }
        }
        if !vec_tables.contains( &String::from( "students")){
            match self.table_create_students(){
                Err(e) => {
                    println!( "Fehler:{:?}\n", e );
                    errors.push_str( format!( "Fehler:{:?}\n", e ).as_str() );
                },
                Ok(_) => (),
            }
        }
        if !vec_tables.contains( &String::from( "absences")){
            match self.table_create_absences(){
                Err(e) => {
                    println!( "Fehler:{:?}\n", e );
                    errors.push_str( format!( "Fehler:{:?}\n", e ).as_str() );
                },
                Ok(_) => (),
            }
        }
        if !vec_tables.contains( &String::from( "grades")){
            match self.table_create_grades(){
                Err(e) => {
                    println!( "Fehler:{:?}\n", e );
                    errors.push_str( format!( "Fehler:{:?}\n", e ).as_str() );
                },
                Ok(_) => (),
            }
        }

        Ok(())
    }

    pub fn read_database_in_vec( &self, course: &String ) -> Result<Vec<Student>> {

        // SQL-Befehl vorbereiten
        let mut stmt = self.conn.prepare(
            "SELECT * FROM students WHERE ( course=:course );",
        )?;

        // SQL-Befehl wird abgearbeitet
        // und alle Schüler als Liste des structs "Student" erstellt
        let students = stmt.query_map(&[(":course", course),
         ],
         |row| {
            Ok(Student {
                id: row.get( "ID" )?,
                surname: row.get( "surname" )?,
                forename: row.get( "forename" )?,
                counter: row.get( "counter" )?,
                course: row.get( "course" )?,
                comment: row.get( "comment" )?,
                grade_type: GradeType::from_string( row.get( "gradetype" )? ),
                place_number: row.get( "placenumber" )?,
            })
        })?;

        let mut vec = Vec::new();

        // Liste der letzten 5 Noten
        let mut vec_last_grades = self.grades_get_last( course ).unwrap();
        // Liste der 2 letzten Abwesenheiten angehängt
        vec_last_grades.append( &mut self.absences_get_last( course ).unwrap() );
        // Nach beiden wird gefiltert - hat also jemand kürzlich eine Note erhalten
        // oder war nicht anwesend,
        // werden diese Schüler:innen nicht ausgewürfelt
        // Durchgehen der Liste und speichern als struct Student im Vector vec
        for student in students{
            let student = student.unwrap();
            if !vec_last_grades.contains( &student.id ) {
                for _i in 0..student.counter
                {
                    // println!("Found student {}", student.name() ); // Debug
                    vec.push( student.clone() );
                }
            }
        }

        // Sollte der Vektor leer sein, so werden alle Schüler:innen
        // der Klasse einfach in eine Liste gepackt.
        if vec.len() == 0 {
            vec = self.read_course_in_vec( course ).unwrap();
        }

        Ok(vec)
    }

    pub fn read_course_in_vec( &self, course: &String )
    -> Result<
        // &String,
        Vec<Student>
    > {
        // SQL-Befehl vorbereiten
        let mut stmt = self.conn.prepare(
            "SELECT * FROM students WHERE ( course=:course );",
        )?;

        // SQL-Befehl wird abgearbeitet
        // und alle Schüler als Liste des structs "Student" erstellt
        let students = stmt.query_map(&[(":course", course),
         // (":fach", fach)
         ],
         |row| {
            Ok(Student {
                id: row.get( "ID" )?,
                surname: row.get( "surname" )?,
                forename: row.get( "forename" )?,
                counter: row.get( "counter" )?,
                course: row.get( "course" )?,
                comment: row.get( "comment" )?,
                grade_type: GradeType::from_string( row.get( "gradetype" )? ),
                place_number: row.get( "placenumber" )?,
            })
        })?;

        let mut vec = Vec::new();
        // Durchgehen der Liste und speichern als struct Student im Vector vec
        for student in students{
            // let student = student.unwrap();
            vec.push( student.unwrap().clone() );
        }

        Ok(vec)
        // match Vec.
    }

    // Wenn ein Schüler abwesend ist, erhöht sich sein Zähler, sodass er beim nächsten Mal
    // wahrscheinlicher abgefragt wird.
    // Gleichzeitig wird ein Vermerk in der Tabelle "absences" gemacht, sodass die Abwesenheit
    // getrackt wird.
    pub fn student_absent( &self, student: &Student, date: &u32 ) -> Result<()>{
        let mut student = student.to_owned();
        if !(student.counter > 32){
            student.counter = student.counter*2
        }
        println!( "Schüler {} {} {} ist abwesend. Zähler steht bei {}.", student.forename, student.surname, student.id, student.counter);
        self.conn.execute(
            "UPDATE students set counter = ?1 WHERE ID = ?2;",
            ( &student.counter, &student.id ),
        )?;
        self.conn.execute(
            "INSERT INTO absences ( studentID, course, date ) values (?1, ?2, ?3);",
            ( &student.id, &student.course, date ),
        )?;

        Ok(())
    }

    pub fn absences_get( &self ) -> Result<Vec<Absence>>{
        let mut stmt = self.conn.prepare(
            "SELECT * FROM 'absences' ORDER BY date DESC;",
        )?;
        let absences = stmt.query_map([], |row| {
            Ok(
                Absence{
                    id: row.get( "ID" )?,
                    student_id: row.get( "studentID" )?,
                    date: row.get( "date" )?,
                    course: String::from( "course" ),
                    surname: String::from( "" ),
                    forename: String::from( "" ),
                }
            )
        })?;

        let mut vec = Vec::new();
        for absence in absences{
            let mut absence = absence.unwrap();
            ( absence.forename, absence.surname, absence.course ) = self.student_get_name_from_id( absence.student_id ).unwrap();
            println!( "{:?}", absence);
            vec.push( absence );
        }

        Ok( vec )
    }

    // Holt die letzten 2 Abwesenheiten des Kurses aus der Datenbank
    // und steckt sie in einen Vec<u32>
    pub fn absences_get_last( &self, course: &String ) -> Result<Vec<u32>>{
        let command: String = format!( "SELECT DISTINCT studentID FROM 'absences' WHERE course='{}' LIMIT 2", course );
        let mut stmt = self.conn.prepare(
            command.as_str()
        )?;
        let absences_id = stmt.query_map([], |row| {
            Ok(
                row.get( "studentID" )?
            )
        })?;

        let mut vec = Vec::new();
        for id in absences_id{
            vec.push( id.unwrap() );
        }

        Ok( vec )
    }

    // Deletes an entry from the absences table if exists.
    pub fn absences_remove( &self, id: u32 ) -> Result<()>{
        // let result =
        self.conn.execute(
            "DELETE FROM absences WHERE ID = ?1;",
            [&id]
        )?;
        Ok(())
    }

    pub fn courses_get( &self ) -> Result<Vec<String>>{
        // let conn = Connection::open( database_path )?;

        let mut stmt = self.conn.prepare(
            "SELECT DISTINCT course FROM 'students' ORDER BY course ASC;",
        )?;

        let courses = stmt.query_map([], |row| {
            Ok(
                row.get( "course" )?,
            )
        })?;

        let mut vec: Vec<String> = Vec::new();
        // Durchgehen der Liste und speichern als struct Student im Vector vec
        for course in courses{
            vec.push( course.unwrap() );
        }

        Ok(vec)
    }

    pub fn grades_get( &self, course: &String ) -> Result<Vec<Grade>>{
        // let conn = Connection::open( database_path )?;
        let mut command: String;
        if course == "" {
            command = format!("SELECT * FROM 'grades' ORDER BY date DESC;" );
        }
        else {
            command = format!("SELECT * FROM 'grades' WHERE course={} ORDER BY date DESC;", course );
        }

        let mut stmt = self.conn.prepare(
            command.as_str()
        )?;

        // Durchgehen des Querys und eintragen in faecher
        let grades = stmt.query_map(
            [],
            |row| {
            Ok(Grade {
                id: row.get( "ID" )?,
                surname: String::from(""),//row.get( "surname" )?,
                forename: String::from(""),//row.get( "forename" )?,
                student_id: row.get( "studentID" )?,
                grade: row.get( "grade" )?,
                type_of_grade: row.get( "gradetype" )?,
                date: row.get( "date" )?,
                course: row.get( "course" )?,
                comment: row.get( "Notiz" )?,
            })
        })?;

        let mut vec: Vec<Grade> = Vec::new();
        // Durchgehen der Liste und speichern als struct Student im Vector vec
        for grade in grades{
            let mut grade = grade.unwrap();
            println!( "Gefunden: {:?}", grade );
            // Nachname und Vorname aus Datenbank auslesen
            let command = format!("SELECT * from students where ID={};", grade.id);
            let mut stmt = self.conn.prepare(
                command.as_str()
            )?;
            let test = stmt.query_map( [], |row| {
                Ok( Student{
                    id: row.get( "ID" )?,
                    surname: row.get( "surname" )?,
                    forename: row.get( "forename" )?,
                    counter: row.get( "counter" )?,
                    course: row.get( "course" )?,
                    comment: row.get( "comment" )?,
                    grade_type: GradeType::from_string( row.get( "gradetype" )? ),
                    place_number: row.get( "placenumber" )?,
                })
            })?;
            for t in test{
                let t = t.unwrap();
                grade.forename = t.forename;
                grade.surname = t.surname;
            }
            // Dem Vektor hinzufügen
            vec.push( grade );
        }
        Ok(vec)
    }

    // Macht einen Eintrag in der Tabelle "grades" mit date, grade, Art der grade (Abfrage, mündlicher Beitrag,...),
    // und ID des Schülers.
    // Gleichzeitig wird sein Zähler herabgesetzt, aber minimal auf 0.
    pub fn grades_new( &self, grade: &Grade, student: &Student ) -> Result<()>{
        println!( "Neuer Eintrag: {:?}", grade );

        // let conn = Connection::open( database_path )?;

        // grade in Tabelle "grades" schreiben
        self.conn.execute(
            "INSERT INTO grades ( date, grade, gradetype, studentID, course, Notiz ) VALUES ( ?1, ?2, ?3, ?4, ?5, ?6 );",
            (&grade.date, &grade.grade, &grade.type_of_grade, &grade.student_id, &grade.course, &grade.comment),
        )?;

        // Zähler halbieren, wenn nicht gerade 1 oder schlechte Note
        let counter: u32;// = 0;
        if student.counter == 1 {
            counter = 1;
        }
        else if grade.grade <= 4.0{
            counter = student.counter/2;
        }
        else {
            counter = student.counter;
        }
        // Neuer Zähler in Tabelle updaten
        self.conn.execute(
            "UPDATE students SET counter = ?1 WHERE ID = ?2;",
            ( &counter, &student.id ),
        )?;

        Ok(())
    }

    pub fn grades_edit_grade( &self, grade: Grade ) -> Result<()>{
        self.conn.execute(
            "UPDATE grades SET grade=?1, gradetype=?2, Notiz=?3 WHERE ID=?4;",
            (&grade.grade, &grade.type_of_grade, &grade.comment, &grade.id)
        )?;
        Ok(())
    }

    pub fn grades_delete_grade( &self, grade: &Grade ) -> Result<()>{
        self.conn.execute(
            "DELETE FROM grades WHERE ID=?1;",
            [ &grade.id ]
        )?;
        Ok(())
    }

    pub fn student_new(
        &self,
        student: &Student//,
        // comment: &String
    ) -> Result<()>{
        let result = self.conn.execute(
            "INSERT INTO students (surname, forename, course, comment, gradetype, placenumber) VALUES (?1, ?2, ?3, ?4, ?5, ?6);",
            // ( &surname, &forename, &course, &class, &subject, &comment),
            ( &student.surname, &student.forename, &student.course, &student.comment, &student.grade_type.to_string(), 0 ),
        )?;
        println!( "{:?}", result);
        Ok(())
    }

    pub fn student_get_name_from_id( &self, id: u32 ) -> Result<( String, String, String)>{
        let mut stmt = self.conn.prepare(
            format!( "SELECT DISTINCT forename, surname, course FROM 'students' WHERE ID={};",
            id ).as_str()
        )?;

        let students = stmt.query_map([], |row| {
            Ok( (
                    row.get( "forename" )?,
                    row.get( "surname" )?,
                    row.get( "course" )?,
                 )
            )
        })?;

        let mut student = ( String::from( "forename" ), String::from( "surname" ), String::from( "course" ) );
        for i in students{
            student = i.unwrap();
        }

        Ok( student )
    }

    // Funktion, die die letzten 5 IDs der Studenten eines Kurses ausliest, die eine Note erhalten
    // haben.
    // Dies wird verwendet, um diese Schüler:innen aus der Liste der Abzufragenden zu streichen, damit eine Person
    // nicht dauernd nacheinander abgefragt wird.
    pub fn grades_get_last( &self, course: &String) -> Result<Vec<u32>>{
        let command: String = format!("SELECT DISTINCT studentID FROM 'grades' WHERE course='{}' LIMIT 5;", course );
        let mut stmt = self.conn.prepare(
            command.as_str()
        )?;

        // Durchgehen des Querys und eintragen in faecher
        let student_ids = stmt.query_map(
            [],
            |row| {
            Ok(
                row.get( "studentID" )?,
            )
        })?;
        let mut vec: Vec<u32> = Vec::new();

        for id in student_ids{
            vec.push( id.unwrap() );
        }

        Ok(vec)
    }

    // Wählt aus einer Liste eine:n beliebige:n Schüler:in aus
    // und gibt das struct Student zurück.
    // Fehlend:
    // - Überprüfung, ob schon abgefragt wurde
    //      1. Liste der Noten -> gefiltert nach Klasse
    //      2. Ist gewählter Schüler in den letzten 5 Malen dabei?
    // - Überprüfung, ob abwesend letztes Mal
    //      1. Liste der Abwesenheiten -> gefiltert nach Klasse
    //      2. Ist gewählter Schüler in den letzten 5 Malen dabei?
    // Überlegung: direkt in Bau des Vektors implementieren!
    // Später: Prüfung anhand des Stundenplans. -> Abwesenheit wird da eingetragen.
    pub fn select_random_student( vec: &Vec<Student> )//klasse: &String, fach: &str )
    -> Student {

        if vec.len() == 0{
            println!( "Länge kleiner als 0!" );
            return Student::default();
        }

        //let len = vec.len()-1;
        let secret_number = rand::thread_rng().gen_range(0..=vec.len()-1);
        let student = vec[ secret_number ].clone();
        println!( "Gewählt wurde: {:?}", vec[ secret_number ]);
        student

    }
}
