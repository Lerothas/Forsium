// SPDX-FileCopyrightText: 2022 Stefan W. 'Lerothas' Draxlbauer <lerothas@mailbox.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
use serde::Deserialize;
use serde::Serialize;

#[derive(Debug,Clone, Copy, PartialEq, Eq, Deserialize,Serialize)]
pub enum GradeType{
    Noten,
    Punkte,
}
impl Default for GradeType{
    fn default() -> GradeType{
        GradeType::Noten
    }
}
#[allow(dead_code)]
impl GradeType{
    pub const ALL: [GradeType; 2] = [
        GradeType::Noten,
        GradeType::Punkte,
    ];

    pub fn from_string( string: String ) -> GradeType{
        if string.eq( "Punkte" ){
            GradeType::Punkte
        }
        else{
            GradeType::Noten
        }
    }
    pub fn to_string( &self ) -> String{
        match self {
            GradeType::Noten => String::from( "Noten" ),
            GradeType::Punkte => String::from( "Punkte" ),
        }
    }
}
impl std::fmt::Display for GradeType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                GradeType::Noten => "Noten",
                GradeType::Punkte => "Punkte",
            }
        )
    }
}

#[derive(Debug,Clone, Copy, PartialEq, Eq, Deserialize,Serialize)]
pub enum GradeScale{
    Einfach,
    Halbzahlig,
    Drittelzahlig,
    Komplett,
}

impl Default for GradeScale{
    fn default() -> GradeScale{
        GradeScale::Halbzahlig
    }
}

impl GradeScale{
    pub const ALL: [GradeScale; 4] = [
        GradeScale::Einfach,
        GradeScale::Halbzahlig,
        GradeScale::Drittelzahlig,
        GradeScale::Komplett,
    ];
    //     pub fn einfach() -> Vec<Vec<str>>{
    //     vec![
    //         vec![ "1.0" , "2.0" , "3.0" , "4.0" , "5.0" , "6.0" ]
    //     ]
    // }
    //
    // pub fn halbzahlig() -> Vec<Vec<str>>{
    //     vec![
    //         vec![ "1.0" , "2.0" , "3.0" , "4.0" , "5.0" , "6.0" ],
    //         vec![ "1.5" , "2.5" , "3.5" , "4.5" , "5.5" ]
    //     ]
    // }
    //
    // pub fn drittelzahlig() -> Vec<Vec<str>>{
    //     vec![
    //         vec![ "1.0" , "2.0" , "3.0" , "4.0" , "5.0" , "6.0" ],
    //         vec![ "1.3" , "1.7" , "2.3" , "2.7" , "3.3" , "3.7" , "4.3" , "4.7" , "5.3" , "5.7" ]
    //     ]
    // }
    //
    // pub fn komplett() -> Vec<Vec<str>>{
    //     vec![
    //         vec![ "1.0" , "2.0" , "3.0" , "4.0" , "5.0" , "6.0" ],
    //         vec![ "1.5" , "2.5" , "3.5" , "4.5" , "5.5" ],
    //         vec![ "1.3" , "1.7" , "2.3" , "2.7" , "3.3" , "3.7" , "4.3" , "4.7" , "5.3" , "5.7" ]
    //     ]
    // }
    //
    // pub fn oberstufe() -> Vec<Vec<str>>{
    //     vec![
    //         vec![ "0.0" , "1.0" , "2.0" , "3.0" , "4.0" ],
    //         vec![ "5.0" , "6.0" , "7.0" , "8.0" , "9.0" ],
    //         vec![ "10.0" , "11.0" , "12.0" , "13.0" , "14.0" , "15.0" ]
    //     ]
    // }

    // pub fn einfach() -> Vec<Vec<String>>{
    //     vec![
    //         vec![ String::from( "1.0" ), String::from( "2.0" ), String::from( "3.0" ), String::from( "4.0" ), String::from( "5.0" ), String::from( "6.0" ) ]
    //     ]
    // }
    //
    // pub fn halbzahlig() -> Vec<Vec<String>>{
    //     vec![
    //         vec![ String::from( "1.0" ), String::from( "2.0" ), String::from( "3.0" ), String::from( "4.0" ), String::from( "5.0" ), String::from( "6.0" ) ],
    //         vec![ String::from( "1.5" ), String::from( "2.5" ), String::from( "3.5" ), String::from( "4.5" ), String::from( "5.5" ) ]
    //     ]
    // }
    //
    // pub fn drittelzahlig() -> Vec<Vec<String>>{
    //     vec![
    //         vec![ String::from( "1.0" ), String::from( "2.0" ), String::from( "3.0" ), String::from( "4.0" ), String::from( "5.0" ), String::from( "6.0" ) ],
    //         vec![ String::from( "1.3" ), String::from( "1.7" ), String::from( "2.3" ), String::from( "2.7" ), String::from( "3.3" ), String::from( "3.7" ), String::from( "4.3" ), String::from( "4.7" ), String::from( "5.3" ), String::from( "5.7" ) ]
    //     ]
    // }
    //
    // pub fn komplett() -> Vec<Vec<String>>{
    //     vec![
    //         vec![ String::from( "1.0" ), String::from( "2.0" ), String::from( "3.0" ), String::from( "4.0" ), String::from( "5.0" ), String::from( "6.0" ) ],
    //         vec![ String::from( "1.5" ), String::from( "2.5" ), String::from( "3.5" ), String::from( "4.5" ), String::from( "5.5" ) ],
    //         vec![ String::from( "1.3" ), String::from( "1.7" ), String::from( "2.3" ), String::from( "2.7" ), String::from( "3.3" ), String::from( "3.7" ), String::from( "4.3" ), String::from( "4.7" ), String::from( "5.3" ), String::from( "5.7" ) ]
    //     ]
    // }
    //
    // pub fn oberstufe() -> Vec<Vec<String>>{
    //     vec![
    //         vec![ String::from( "0.0" ), String::from( "1.0" ), String::from( "2.0" ), String::from( "3.0" ), String::from( "4.0" ) ],
    //         vec![ String::from( "5.0" ), String::from( "6.0" ), String::from( "7.0" ), String::from( "8.0" ), String::from( "9.0" ) ],
    //         vec![ String::from( "10.0" ), String::from( "11.0" ), String::from( "12.0" ), String::from( "13.0" ), String::from( "14.0" ), String::from( "15.0" ) ]
    //     ]
    // }

    // pub fn scale( scale: GradeScale ) -> Vec<Vec<String>>{
    //     match scale {
    //         GradeScale::Einfach => GradeScale::einfach(),
    //         GradeScale::Halbzahlig => GradeScale::halbzahlig(),
    //         GradeScale::Drittelzahlig => GradeScale::drittelzahlig(),
    //         GradeScale::Komplett => GradeScale::komplett(),
    //         GradeScale::Oberstufe => GradeScale::oberstufe(),
    //     }
    // }
}

impl std::fmt::Display for GradeScale {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                GradeScale::Einfach => "Einfach",
                GradeScale::Halbzahlig => "Halbzahlig",
                GradeScale::Drittelzahlig => "In .3-Schritten",
                GradeScale::Komplett => "Komplett",
            }
        )
    }
}

