// SPDX-FileCopyrightText: 2022 Stefan W. 'Lerothas' Draxlbauer <lerothas@mailbox.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::current_date_as_int as current_date_as_int;

// Konstrukt für Noteneintrag
#[derive(Clone,Debug)]
pub struct Grade{
    pub id: u32,
    pub surname: String,
    pub forename: String,
    pub student_id: u32,
    pub grade: f32,
    pub date: u32,
    pub type_of_grade: String,
    pub course: String,
    pub comment: String,
}

impl Default for Grade {
    fn default() -> Grade{
        Grade{
            id: 0,
            surname: String::from(""),
            forename: String::from(""),
            student_id: 0,
            grade: 2.0,
            date: current_date_as_int::current_date_as_int(),
            type_of_grade: String::from("Mündliche Note"),
            course: String::from(""),
            comment: String::from(""),
        }
    }
}

impl Grade{
    pub fn name( &self ) -> String{
        return String::from( format!( "{} {}", &self.forename, &self.surname ) )
        .clone()
    }

    pub fn grade( &self ) -> String {
        return self.grade.to_string().clone();
    }

    pub fn date( &self ) -> String {
        let s = self.date.to_string();
        let v: Vec<&str> = s.split("").collect();
        // println!("{:?} -> {:?} -> {:?}", self.date, s, v);
        let string =
            String::from( v[7] )
            + v[8]
            + "."
            + v[5]
            + v[6]
            + "."
            + v[1]
            + v[2]
            + v[3]
            + v[4]
            ;
        return string.clone();
    }
}

impl std::fmt::Display for Grade {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!( f, "{} {}: Note {} am {}; '{}'", &self.forename, &self.surname, &self.grade, &self.date(), &self.comment )
    }
}
