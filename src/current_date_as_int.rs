// SPDX-FileCopyrightText: 2022 Stefan W. 'Lerothas' Draxlbauer <lerothas@mailbox.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use chrono::{DateTime, Utc};

pub fn current_date_as_int() -> u32{
        let now: DateTime<Utc> = Utc::now();
        let date: u32 = match now.format("%Y%m%d").to_string().parse() {
            Ok(num) => num,
            Err(_) => 0,
        };
        date
}
