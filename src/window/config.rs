// SPDX-FileCopyrightText: 2022 Stefan W. 'Lerothas' Draxlbauer <lerothas@mailbox.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::fs;
use std::fs::File;
use std::path::Path;
use std::io::prelude::*;
use std::env::var;
use toml;
use serde::Deserialize;
use serde::Serialize;
use toml::to_string;

use crate::window::palette::CustomPalette as CustomPalette;
use crate::gradescale::GradeScale as GradeScale;
use crate::gradescale::GradeType as GradeType;

#[derive(Debug, Deserialize,Serialize)]
pub struct Config{
    pub theme: String,
    pub history: Vec<String>,
    pub gradetype: Option<GradeType>,
    pub gradescale: GradeScale,

}
impl Default for Config {
    fn default() -> Config{
        Config{
            theme: format!( "{}", CustomPalette::Light),
            history: Vec::new(),
            gradetype: Some( GradeType::default() ),
            gradescale: GradeScale::default(),
        }
    }
}
impl Config{
    pub fn history_add( &mut self, string: &String ){
        if !self.history.contains( string ){
            self.history = vec![
                if self.history.len() > 2{
                    self.history[ self.history.len()-2 ].clone()
                }
                else{
                    String::from("")
                },
                if self.history.len() > 1{
                    self.history[ self.history.len()-1 ].clone()
                }
                else{
                    String::from("")
                },
                string.to_owned()
            ];
        }
    }
}

const APP_NAME: &str = "forsium";

pub fn create( path: &str ) -> bool {
    let result = create_folder( &String::from( path ) );
    match result{
        Ok(_) => {
            let path_config = path.to_owned() + "/"+ &APP_NAME +"/config.toml";
            if !Path::new( &path_config ).exists() {
                // let mut file = File::create( path_config.clone() ).unwrap();
                let config = Config::default();
                // let result =
                let _ = write_to_file( &config, path_config.as_str() );
                // println!( "{:?}", result );
                true
            }
            else {
                false
            }
        },
        Err( e ) => {
            println!( "{:?}", e );
            false
        }
    }
}

pub fn create_folder( path: &String ) -> std::io::Result<()> {
    let path: String = String::from("") + path.as_str().clone() + "/"+ APP_NAME;
    // if !does_folder_foo_exist_in_current_directory( &path ).unwrap() {
    // let result = Path::new( &String::from( path.clone() ) ).exists();
    // println!( "Ordner {} existiert: {:?}", path, result );
    if !Path::new( &String::from( path.clone() ) ).exists() {
        fs::create_dir( path )?;
        Ok(())
    }
    else{
        Ok(())
    }

}

pub fn write_to_file(config: &Config, file_path: &str) -> Result<(), Box<dyn std::error::Error>> {
    let toml_string = to_string(config)?;
    let mut file = File::create(file_path)?;
    file.write_all(toml_string.as_bytes())?;
    Ok(())
}

pub fn read( path: &str ) -> Config {
    if !Path::new( &String::from( path.clone() ) ).exists(){
        let home = var("XDG_CONFIG_HOME")
        .or_else(|_| var("HOME").map(|home|format!("{}/.config", home))).unwrap();
        create( home.as_str() );
    }
    let toml_str = fs::read_to_string( path ).expect("Failed to read config.toml file");
    let cargo_toml: Config = toml::from_str(&toml_str).expect("Failed to deserialize config.toml");

    // println!("{:#?}", cargo_toml);
    cargo_toml
}

pub fn path() -> String{
    let home = var("XDG_CONFIG_HOME")
        .or_else(|_| var("HOME").map(|home|format!("{}/.config", home))).unwrap();
    // println!("Config-Home-Verzeichnis: {:?}", home);
    String::from("") + home.as_str().clone() + "/"+ APP_NAME + "/config.toml"
}

// fn does_folder_foo_exist_in_current_directory( path: &String ) -> Result<bool, io::Error> {
//     // let cur_path_buf = env::current_dir()?;
//     let cur_dir = Path::new( path );
//     Ok(fs::read_dir(cur_dir)?.find(|x| {
//         let x = x.as_ref().unwrap();
//         x.file_type().unwrap().is_dir() && x.file_name().to_str().unwrap() == path.as_str().to_owned()
//     }).is_some())
// }
