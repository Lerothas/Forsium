// SPDX-FileCopyrightText: 2022 Stefan W. 'Lerothas' Draxlbauer <lerothas@mailbox.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use iced::Font;
use iced::widget::Text;
use iced_aw::graphics::icons::*;

// Fonts
// const ICONS: Font = Font{
//     family: iced::font::Family::Name("icons"),
//     weight: iced::font::Weight::Normal,
//     stretch: iced::font::Stretch::Normal,
//     // style: iced::font::Style::Normal,
//     monospaced: false,
// };
// const ICONS: Font = Font::External {
//     name: "Icons",
//     bytes: include_bytes!("icons.ttf"),
// };

fn icon(unicode: char) -> Text<'static> {
    iced::widget::text(unicode.to_string())
        // .font(ICONS)
        .width(30)
        .horizontal_alignment(iced::alignment::Horizontal::Center)
        .size(30)
}

pub fn skip() -> Text<'static> {
    icon('\u{E81D}')
}
pub fn absent() -> Text<'static> {
    icon('\u{E821}')
}
pub fn cancel() -> Text<'static> {
    icon('\u{E806}')
}
pub fn add_grade() -> Text<'static> {
    icon('\u{E813}')
}
// pub fn open() -> Text<'static>{
//     icon( '\u{E80D}' )
// }
pub fn settings() -> Text<'static>{
    icon( '\u{E814}' )
}
pub fn grade_list() -> Text<'static>{
    icon( '\u{E822}' )
}
pub fn add_student() -> Text<'static>{
    icon( '\u{E811}' )
}
pub fn import() -> Text<'static>{
    icon( '\u{E808}' )
}
pub fn back() -> Text<'static>{
    icon( '\u{E800}' )
}
pub fn forward() -> Text<'static>{
    icon( '\u{E801}' )
}
pub fn absences_list() -> Text<'static>{
    icon( '\u{E818}' )
}
pub fn courses_list() -> Text<'static>{
    icon( '\u{E820}' )
}
pub fn remove() -> Text<'static>{
    icon( '\u{E80B}' )
}

pub fn delete_icon() -> Text<'static> {
    iced::widget::text( icon_to_char( Icon::TrashFill ) )
}
