// SPDX-FileCopyrightText: 2022 Stefan W. 'Lerothas' Draxlbauer <lerothas@mailbox.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

// mod select_grade_widget;

// Imports for database, student and time
// use crate::database as db;
use crate::database::*;
use crate::student::Student;
use crate::grade::Grade;
use crate::absence::Absence;
use crate::current_date_as_int as current_date_as_int;
// use chrono::{DateTime, Utc};
// use std::convert::TryInto;
//use once_cell::sync::Lazy;

// Iced specific imports
use iced::{Settings, window, Application, Command, Alignment,
    Font,
};
use iced::executor;
// use iced::time;
use iced::widget::{Button, Column, Container, Text, Row, TextInput, Tooltip, tooltip::Position, Scrollable, Rule, Space
    //, Toggler
    , Radio
    , PickList};
use iced::theme::{self, Theme};
use iced_aw::native::Grid;
use iced_aw::modal::Modal as Modal;
use iced_aw::card::Card as Card;
use iced_aw::floating_element::FloatingElement;
use rfd::FileDialog;
use std::env::var;

mod icons;
mod palette;
mod config;

use crate::window::config::Config as Config;
use self::palette::CustomPalette;
use crate::gradescale::GradeScale as GradeScale;
use crate::gradescale::GradeType as GradeType;

// const ANDIKA: Font = Font::External {
//     name: "Andika",
//     bytes: include_bytes!("Andika-Regular.ttf"),
// };
// static ICON: &[u8] = include_bytes!("../icon.png");
// const ICON_HEIGHT: u32 = 128;
// const ICON_WIDTH: u32 = 128;

const PADDING: f32 = 10.0;

pub fn run_gui() -> Result<(), iced::Error> {
    //let image = image::load_from_memory(ICON).unwrap();
    // let icon = iced::window::Icon::from_rgba(image.as_bytes().to_vec(), ICON_HEIGHT, ICON_WIDTH).unwrap();

    ForsiumMainWindow::run( Settings {
        window: window::Settings {
            // icon: Some(icon),
            size: (1200, 600),
            min_size: Some( (800, 600) ),
            ..window::Settings::default()
        },
        // default_font: iced::font::load( include_bytes!("CharisSIL-Regular.ttf") ),
        ..Settings::default()
    } )
}

struct ForsiumMainWindow {
    config: Config,
    state: Layer,
    database_path: String,
    database: Database,
    courses: Vec<String>,
    course_selected: String,
    modal_state_grade_taking: LayerCoursePage,
    grade: Grade,
    gradetype: Option<GradeType>,
    gradescale: GradeScale,
    grades_list: Vec<Grade>,
    modal_state_grade_edit: LayerGradePage,
    student: Student,
    modal_state: bool,
    absences_list: Vec<Absence>,
    absence: Absence,
    error: String,
}

#[derive(Debug, Clone)]
enum Message {
    StateChanged( Layer ),
    // Messages für die Datenbank
    // DatabasePathChanged(String),
    DatabasePathFileDialog,
    // DatabasePWChanged(String),
    DatabaseCreateNew,
    DatabaseHistoryClicked( String ),
    // Messages für den Hauptteil des Programms
    CourseSelected(String),
    Skip,
    Absent,
    SetGrade,
    GradeChanged(f32),
    GradeTypeChanged( GradeType ),
    GradeScaleDefaultChanged( GradeScale ),
    CommentChanged(String),
    // GradeScale(String),
    DatabaseImportFromCSV,
    // Messages zum Einfügen neuer Schüler
    CoursesAddWindowShow,
    CoursesAddWindowClose,
    CoursesAddWindowAccept,
    StudentInputChangedForename( String ),
    StudentInputChangedSurname( String ),
    StudentInputChangedCourse( String ),
    StudentInputChangedComment( String ),
    // Messages für die Einstellungen
    SettingsThemeDarkMode( CustomPalette ),
    // Messages für die Notenliste
    GradeListChangeButtonClicked( Grade ),
    GradeListChangeCardAccept,
    GradeListChangeCardCancel,
    GradeListChangeCardDelete,
    GradeListChangeCardDeleteAccept,
    GradeListChangeCardDeleteCancel,
    // Messages für die Abwesenheiten-Liste
    AbsencesListDeleteButtonClicked( Absence ),
    AbsencesListDeleteCardCancel,
    AbsencesListDeleteCardAccept( u32 ),
}

#[derive(Debug, Clone)]
pub enum Layer {
    SelectDatabase,
    SelectCourse,
    // NewStudent,
    ShowGrades,
    ShowAbsences,
    Settings,
}

#[derive(Debug, Clone)]
pub enum LayerCoursePage{
    NoModal,
    AddStudent,
    GradeTaking,
}
#[derive(Debug, Clone)]
pub enum LayerGradePage{
    NoModal,
    EditGrade,
    DeleteGrade,
}

impl Application for ForsiumMainWindow {
    type Message = Message;
    type Theme = iced::Theme;
    type Executor = executor::Default;      // Das ist wohl notwendig für den Timer!
    type Flags = ();                        // Keine Ahnung, warum das notwendig ist...

    fn new( _flags: () ) -> (Self, Command<Message>) {
        let configuration = config::read( config::path().as_str() );
        (
            ForsiumMainWindow {
                student: Student::default(),
                course_selected: "".into(),
                state: Layer::SelectDatabase,
                modal_state: false,
                modal_state_grade_taking: LayerCoursePage::NoModal,
                modal_state_grade_edit: LayerGradePage::NoModal,
                courses: Vec::new(),
            	grade: Grade::default(),
                gradetype: Some( GradeType::default() ),
                gradescale: configuration.gradescale,
                database_path: String::from( "" ),
                database: Database::open_connection( &String::from("") ),
                grades_list: Vec::new(),
                absences_list: Vec::new(),
                absence: Absence::default(),
                error: String::from(""),
                config: configuration,
            },
            Command::none(),
        )
    }

    fn title(&self) -> String {
        String::from( "Forsium" ) + " - " + &self.database.return_path()
    }

    fn update(&mut self, message: Self::Message) -> Command<Message> {
        match message {
            Message::CourseSelected(course) => {
                // self.gradescale = self.config.gradescale;
                self.course_selected = course.clone();
                self.grade.course = course;
                // self.state = Layer::GradeTaking;
                self.modal_state_grade_taking = LayerCoursePage::GradeTaking;
                // SchGradeScaleritt: Löschen des Vektors .clear() -> Neu mit der Vectorliste
                // Würde Rechenarbeit sparen.
                self.student = Database::select_random_student(
                    &self.database.read_database_in_vec(
                        &String::from( self.course_selected.clone() )
                    ).unwrap()
                );
                self.grade.student_id = self.student.id;
                self.grade.forename = self.student.forename.clone();
                self.grade.surname = self.student.surname.clone();
                println!( "{:?}", self.grade );
            },
            Message::Skip => {
                self.student = Database::select_random_student(
                    &self.database.read_database_in_vec(
                        &String::from( self.course_selected.clone() )
                    ).unwrap()
                );
            },
            Message::Absent => {
                let result = self.database.student_absent(
                    &self.student,
                    &current_date_as_int::current_date_as_int()
                );
                match result {
                    Err(e) => self.error = format!( "Fehler: {:?}\n Eintrag nicht getätigt.", e),
                    Ok(()) => (),
                }
                self.student = Database::select_random_student(
                    &self.database.read_database_in_vec(
                        &String::from( self.course_selected.clone() )
                    ).unwrap()
                );
            },
            Message::SetGrade => {
                let result = self.database.grades_new(
                    &self.grade,
                    &self.student,
                );
                //println!("{:?}", result);     // Debug Output
                match result {
                    Err(e) => {
                        println!("{:?}", e);
                        self.error = format!{"Fehler: {:?}\n Eintragung nicht getätigt.", e};
                    },
                    Ok(_) => {
                        self.modal_state_grade_taking = LayerCoursePage::NoModal;
                        self.grade.grade = 2.0;
                        self.grade.comment = String::from( "" );
                    },
                }

            },
            Message::GradeChanged(grade_input) =>{
                self.grade.grade = grade_input;
            },
            Message::GradeTypeChanged( gradetype ) => {
                self.student.grade_type = gradetype;
                self.gradetype = Some( gradetype );
            },
            Message::GradeScaleDefaultChanged( scale ) => {
                println!( "Standard geändert auf: {}", scale );
                self.config.gradescale = scale;
            },
            Message::DatabaseCreateNew => {
                let files = FileDialog::new()
                    .set_title( "Wählen Sie eine Forsium-Datenbank" )
                    .add_filter( "sqlite", &["forsium", "fs"] )
                    .set_directory( var("XDG_HOME")
                    .or_else(|_| var("HOME")).unwrap() )
                    .set_file_name( "Schuljahr.forsium" )
                    .save_file();
                if !files.is_none(){
                    self.database = Database::open_connection(
                    //db::create_database::create(
                        &files.unwrap()
                        .into_os_string()
                        .into_string()
                        .unwrap()
                        //.as_str()
                    );
                    self.database_path = self.database.return_path();
                    // match self.database.check_database() {
                    //     Err(e) => self.error = format!( "Fehler: {:?}", e),
                    //     Ok(()) => {
                    //         self.database = Database::open_connection( &self.database_path );
                            match self.database.check_database(){
                                Err(e) => {
                                    println!( "Fehler: {:?}", e );
                                    self.error = format!( "Fehler: {:?}", e);
                                },
                                Ok(_) => (),
                            }
                            let result = self.database.courses_get();
                            match result {
                                Err(e) => {
                                    println!("{:?}", e);
                                    self.error = format!("{} {:?}",self.database_path, e);
                                    self.state = Layer::SelectDatabase;
                                }
                                Ok(v) => {
                                    // println!("Ergebnis: {:?}", v);
                                    self.courses = v;
                                    self.grades_list = self.database.grades_get( &String::from("") ).unwrap();
                                    self.state = Layer::SelectCourse;
                                }
                            }
                        // },
                    // }
                    println!( "{}", self.database.return_path() );
                }
            },
            Message::DatabaseHistoryClicked( string ) => {
                self.database_path = string.as_str().into();
                self.database = Database::open_connection( &self.database_path );
                match self.database.check_database(){
                    Err(e) => {
                        println!( "Fehler: {:?}", e );
                        self.error = format!( "Fehler: {:?}", e);
                    },
                    Ok(_) => (),
                }
                let result = self.database.courses_get();
                match result {
                    Err(e) => {
                        println!("{:?}", e);
                        self.error = format!("{} {:?}",self.database_path, e);
                        self.state = Layer::SelectDatabase;
                    }
                    Ok(v) => {
                        // println!("Ergebnis: {:?}", v);
                        self.courses = v;
                        self.grades_list = self.database.grades_get( &String::from("") ).unwrap();
                        self.state = Layer::SelectCourse;
                    }
                }
            },
            Message::DatabaseImportFromCSV => {
                let files = FileDialog::new()
                    .set_title( "Wählen Sie eine CSV-Datei für den Import" )
                    .add_filter( "sqlite", &["csv" ] )
                    .set_directory( var("XDG_HOME")
                    .or_else(|_| var("HOME")).unwrap() )
                    .pick_file();
                if !files.is_none(){
                    match importcsv::import_csv(
                        &files.unwrap().into_os_string().into_string().unwrap(),
                        &self.database
                    ){
                        Err( e ) => self.error = format!( "Fehler: {:?}", e),
                        Ok( _ ) => {
                            self.modal_state_grade_taking = LayerCoursePage::NoModal;
                            self.courses = self.database.courses_get().unwrap();
                        },
                    };
                }
            },
            Message::CommentChanged( string ) =>{
                self.grade.comment = string.as_str().into();
            },
            Message::DatabasePathFileDialog => {
                let files = FileDialog::new()
                    .set_title( "Wählen Sie eine Forsium-Datenbank" )
                    .add_filter( "sqlite", &["forsium", "fs"] )
                    .set_directory( var("XDG_HOME")
                    .or_else(|_| var("HOME")).unwrap() )
                    .pick_file();
                // println!( "PathBuf {:?}", files );   // Debug
                if !files.is_none(){
                    self.database_path = files.unwrap().into_os_string().into_string().unwrap();

                    self.config.history_add( &self.database_path );
                        let _ = config::write_to_file( &self.config, config::path().as_str() );
                        if self.database.return_path().eq( &String::from("") ){
                            self.database = Database::open_connection( &self.database_path );
                            match self.database.check_database(){
                                Err(e) => {
                                    println!( "Fehler: {:?}", e );
                                    self.error = format!( "Fehler: {:?}", e);
                                },
                                Ok(_) => (),
                            }
                        }
                        let result = self.database.courses_get();
                        match result {
                            Err(e) => {
                                println!("{:?}", e);
                                self.error = format!("{} {:?}",self.database_path, e);
                                self.state = Layer::SelectDatabase;
                            }
                            Ok(v) => {
                                // println!("Ergebnis: {:?}", v);
                                self.courses = v;
                                self.grades_list = self.database.grades_get( &String::from("") ).unwrap();
                                self.state = Layer::SelectCourse;
                            }
                        }
                }
            },
            Message::StateChanged( layer ) => {
                match layer {
                    Layer::SelectDatabase => self.state = layer,
                    Layer::SelectCourse => {
                        self.config.history_add( &self.database_path );
                        let _ = config::write_to_file( &self.config, config::path().as_str() );
                        if self.database.return_path().eq( &String::from("") ){
                            self.database = Database::open_connection( &self.database_path );
                            match self.database.check_database(){
                                Err(e) => {
                                    println!( "Fehler: {:?}", e );
                                    self.error = format!( "Fehler: {:?}", e);
                                },
                                Ok(_) => (),
                            }
                        }
                        let result = self.database.courses_get();
                        match result {
                            Err(e) => {
                                println!("{:?}", e);
                                self.error = format!("{} {:?}",self.database_path, e);
                                self.state = Layer::SelectDatabase;
                            }
                            Ok(v) => {
                                // println!("Ergebnis: {:?}", v);
                                self.courses = v;
                                self.grades_list = self.database.grades_get( &String::from("") ).unwrap();
                                self.state = layer;
                            }
                        }
                    },
                    Layer::ShowGrades => {
                        self.gradescale = self.config.gradescale;
                        self.grades_list = self.database.grades_get( &String::from("") ).unwrap();
                        self.state = layer;
                    },
                    Layer::ShowAbsences => {
                        self.absences_list = self.database.absences_get().unwrap();
                        self.state = layer;
                    },
                    Layer::Settings => self.state = layer,
                }
            },
            Message::CoursesAddWindowShow => {
                self.student.forename = String::from( "" );
                self.student.surname = String::from( "" );
                self.student.comment = String::from( "" );
                self.student.counter = 2;
                self.modal_state_grade_taking = LayerCoursePage::AddStudent;
            },
            Message::CoursesAddWindowClose => self.modal_state_grade_taking = LayerCoursePage::NoModal,
            Message::CoursesAddWindowAccept => {
                self.modal_state_grade_taking = LayerCoursePage::NoModal;
                let result = self.database.student_new(
                    &self.student
                );
                match result {
                    Err(e) => self.error = format!( "Fehler: {:?}", e),
                    Ok(_) => {
                        let result = self.database.courses_get();
                        match result {
                            Err(e) => {
                                println!("{:?}", e);
                                self.error = format!("{} {:?}",self.database_path, e);
                                self.state = Layer::SelectDatabase;
                            }
                            Ok(v) => {
                                println!("Ergebnis: {:?}", v);
                                self.courses = v;
                                self.grades_list = self.database.grades_get( &String::from("") ).unwrap();
                            }
                        }
                    }
                }
            },
            Message::StudentInputChangedForename( string ) => self.student.forename = string,
            Message::StudentInputChangedSurname( string ) => self.student.surname = string,
            Message::StudentInputChangedCourse( string ) => self.student.course = string,
            Message::StudentInputChangedComment( string ) => self.student.comment = string,
            Message::SettingsThemeDarkMode( palette ) => {
                self.config.theme = palette.to_string();
                let _ = config::write_to_file( &self.config, config::path().as_str() );
            },
            Message::GradeListChangeButtonClicked( grade ) => {
                println!( "{:?}", grade);
                self.modal_state_grade_edit = LayerGradePage::EditGrade;
                self.grade = grade;
            },
            Message::GradeListChangeCardAccept => {
                match self.database.grades_edit_grade( self.grade.clone() ){
                    Err( e ) => self.error = format!( "{:?}", e),
                    Ok( _ ) => {
                        self.grades_list = self.database.grades_get( &String::from("") ).unwrap();
                        self.modal_state_grade_edit = LayerGradePage::NoModal;
                    }
                }
            },
            Message::GradeListChangeCardCancel => self.modal_state_grade_edit = LayerGradePage::NoModal,
            Message::GradeListChangeCardDelete => self.modal_state_grade_edit = LayerGradePage::DeleteGrade,
            Message::GradeListChangeCardDeleteAccept => {
                match self.database.grades_delete_grade( &self.grade ){
                    Ok( _ ) => self.modal_state_grade_edit = LayerGradePage::NoModal,
                    Err( e ) => self.error = format!( "Fehler: {:?}", e),
                }
            },
            Message::GradeListChangeCardDeleteCancel => {
                    self.modal_state_grade_edit = LayerGradePage::NoModal;
            },
            Message::AbsencesListDeleteButtonClicked( absence ) => {
                self.absence = absence;
                self.modal_state = true;
            },
            Message::AbsencesListDeleteCardAccept( int )=> {
                match self.database.absences_remove( int ){
                    Err( e ) => self.error = format!( "Fehler: {:?}", e),
                    Ok( _ ) => {
                        self.absences_list = self.database.absences_get().unwrap();
                        self.modal_state = false;
                    },
                }
            },
            Message::AbsencesListDeleteCardCancel => self.modal_state = false,
        }
        Command::none()
    }

    // Erstellt das Theme... es ist so einfach!
    fn theme(&self) -> Theme {
        CustomPalette::theme_from_string( &self.config.theme )
    }

    fn view(&self) -> iced::Element<Self::Message> {
        let text_error = Text::new( self.error.clone() );
        Row::new()
        .push( self.create_sidebar() )
        .push(
            match self.state{
                Layer::SelectDatabase => {
                    self.create_page_select_database()
                },

                // Layer, das die Kurswahl erlaubt
                Layer::SelectCourse => {
                    self.create_page_select_course()
                },

                Layer::ShowGrades => {
                    self.create_page_grade_list()
                },
                Layer::ShowAbsences => {
                    self.create_page_absences_list()
                },
                Layer::Settings => {
                    self.create_page_settings()
                },
            }
        )
        .push( text_error )
        .into()
    }
}

impl ForsiumMainWindow{

    // Diese Funktion erstellt die Seite zur Auswahl der Datenbank (und später des Passworts).
    // Enthält auch ein Dialogfenster zum Erstellen einer neuen Datenbank und eine Liste an bisher geöffneten Datenbanken.
    fn create_page_select_database( &self ) -> iced::Element<'_, Message>{
        let text = Text::new( "Willkommen bei Forsium!" )
        // .font( ANDIKA )
        .size(40);
        let row_database_path = Row::new()
            .push(
                self.button_tooltip(
                    String::from( "Existierende Datenbank öffnen" ),
                    icons::import(),
                    Message::DatabasePathFileDialog,
                    theme::Button::Primary,
                    Position::Top
                )
            )
            // .push(
            //     self.button_tooltip(
            //         String::from( "Öffnen" ),
            //         icons::forward(),
            //         Message::StateChanged( Layer::SelectCourse ),
            //         theme::Button::Positive,
            //         Position::Top
            //     )
            // )
            .push(
                self.button_tooltip(
                    String::from( "Neue Datenbank erstellen" ),
                    icons::add_grade(),
                    Message::DatabaseCreateNew,
                    theme::Button::Secondary,
                    Position::Top
                )
            )
            .align_items( Alignment::Center )
            .spacing( 30 );

        // HISTORY BEGINN
        // Erstellt eine Liste der letzten bis zu drei geöffneten Datenbanken, falls es solche gibt.
        let mut vec_history = Vec::new();
        for history in &self.config.history{
            if !history.eq( "" ){
                vec_history.push(
                    Button::new( history.as_str() )
                    .on_press( Message::DatabaseHistoryClicked( history.clone() ) )
                    .height( 50 )
                    .width( 500 )
                    .padding( 10 )
                    .into()
                );
            }
        }
        let col_history = Column::with_children( vec_history )
            .spacing( 10 )
            .align_items( Alignment::Center );
        // HISTORY ENDE

        // Alles bisherige in einer Spalte zusammenbauen.
        let col = Column::new()
            .push( text )
            .push( Space::with_height( 40 ) )
            .push( row_database_path )
            // .push( row_database_pw )
            .push( col_history )
            .spacing( 10 )
            .padding( 20 )
            .align_items( Alignment::Center );

        // Dialogfenster:
        Container::new(
                col
        )
        .center_x().center_y().width(iced::Length::Fill).height(iced::Length::Fill).into()
    }

    // Diese Funktion erstellt die Seite zur Auswahl des Kurses.
    fn create_page_select_course( &self ) -> iced::Element<'_, Message>{

        let row = self.create_button_courses();

        let space1 = Space::new(10, iced::Length::Fill);
        let space2 = Space::new(10, iced::Length::Fill);

        let col = Column::new()
            .push( self.section( "Kurswahl" ))
            .push( space1 )
            .push(row)
            .push( space2 )
            .spacing( 20 )
            .padding( 20 )
            .align_items( Alignment::Center );

        // Dialogfenster zum Hinzufügen neuer Schüler
        let modal = Modal::new(
            // match self.modal_state_grade_taking{
            //     LayerCoursePage::NoModal => false,
            //     LayerCoursePage::AddStudent => true,
            //     LayerCoursePage::GradeTaking => true,
            // },
            // self.modal_state,
            FloatingElement::new(
                col,
                Tooltip::new(
                        Button::new( icons::add_student() )
                        .on_press( Message::CoursesAddWindowShow )
                        .padding( iced::Padding::from( PADDING ) ),
                        String::from("Schüler hinzufügen"),
                        Position::FollowCursor
                    )
            ),
            Some(
                match self.modal_state_grade_taking{
                    LayerCoursePage::NoModal => self.create_card_student_add(),
                    LayerCoursePage::AddStudent => self.create_card_student_add(),
                    LayerCoursePage::GradeTaking => self.create_card_grade_taking(),
                }
            )
        )
        .backdrop( Message::CoursesAddWindowClose )
        // .on_esc( Message::CoursesAddWindowClose )
        // .into()
        ;

        Container::new( modal ).center_x().width(iced::Length::Fill).height(iced::Length::Fill).into()
    }

    // fn create_page_view_course( &self ) -> iced::Element<'_, Message>{
    //     let col = Column::new()
    //
    //     ;
    //     Container::new(col).center_x().center_y().width(iced::Length::Fill).height(iced::Length::Fill).into()
    // }

    fn create_page_grade_list( &self ) -> iced::Element<'_, Message>{
        // Alle Elemente aus dem Vektor der Noten als Vektor
        let mut vec_element_grades: Vec<iced::Element<'_, Message>> = Vec::new();

        for grade in self.grades_list.clone(){
            let text_forename = Text::new( grade.forename.clone() ).width(100);
            let text_surname = Text::new( grade.surname.clone() ).width(100);
            let text_grade = Text::new( grade.grade() ).width(50);
            let text_type_of_grade = Text::new( grade.type_of_grade.clone() ).width(100);
            let text_date = Text::new( grade.date()  ).width(100);
            let text_course = Text::new( grade.course.clone() ).width(150);
            let text_comment = Text::new( grade.comment.clone() ).width(200);
            let button = Button::new(
                icons::settings()
            )
            .style( theme::Button::Secondary )
            .on_press( Message::GradeListChangeButtonClicked( grade.clone() ));
            let row = Row::new()
                .push( text_forename )
                .push( text_surname )
                .push( text_grade )
                .push( text_type_of_grade )
                .push( text_date )
                .push( text_course )
                .push( text_comment )
                .push( button )
                .spacing( 20 )
                ;
            vec_element_grades.push( row.into() );
            let rule = Rule::horizontal( 10 );
            vec_element_grades.push( rule.into() );
        }
        let col_grades = Column::with_children( vec_element_grades )
            .spacing( 10 )
            .padding( 10 );
        // Scrollable Element, um alle Noten anzuzeigen mit Header
        let scrollable = Scrollable::new( col_grades );
        let header = Row::new()
            .push( Text::new( "Vorname" ).width(100) )
            .push( Text::new( "Nachname" ).width(100) )
            .push( Text::new( "Note" ).width(50) )
            .push( Text::new( "Notenkat." ).width( 100 ))
            .push( Text::new( "Datum"  ).width(100) )
            .push( Text::new( "Kurs" ).width(150) )
            .push( Text::new( "Kommentar" ).width(200) )
            .spacing( 20 )
            .padding( 10 );

        Container::new(
            Modal::new(
                // match self.modal_state_grade_edit{
                //     LayerGradePage::NoModal => false,
                //     LayerGradePage::EditGrade => true,
                //     LayerGradePage::DeleteGrade => true,
                // },
                Column::new()
                .push( self.section( "Liste der Noten" ) )
                .push( header )
                .push( Rule::horizontal( 10 ) )
                .push( scrollable ),
                match self.modal_state_grade_edit{
                    LayerGradePage::NoModal => self.create_card_grade_edit().into(),
                    LayerGradePage::EditGrade => self.create_card_grade_edit().into(),
                    LayerGradePage::DeleteGrade => self.create_card_grade_delete().into(),
                }
            )
            .on_esc( Message::GradeListChangeCardCancel )
            .backdrop( Message::GradeListChangeCardCancel )
        ).center_x().width(iced::Length::Fill).height(iced::Length::Fill).into()
    }

    fn create_card_grade_edit( &self ) -> iced::Element<'_, Message> {
        Card::new(
            Text::new( "Note bearbeiten" ),
            Column::new()
                .push( Text::new( self.grade.name() ) )
                // .push( Text::new(  ) )
                .push( self.create_button_grades() )
                .push( TextInput::new(
                        "Kommentar",
                        self.grade.comment.as_str()
                    )
                    .on_input( Message::CommentChanged )
                )
                .spacing( 20 )
                .align_items( Alignment::Center )
        ).foot(
            Row::new()
            .push(
                Button::new( icons::back()  )
                .style( theme::Button::Secondary )
                .on_press( Message::GradeListChangeCardCancel )
                .padding( 10 )
            )
            .push(
                Button::new( icons::delete_icon() )
                .on_press( Message::GradeListChangeCardDelete )
                .style( theme::Button::Destructive )
                .padding( 10 )
            )
            .push( Space::with_width( iced::Length::Fill ))
            .push(
                Button::new( icons::add_grade() )
                .padding( 10 )
                .on_press( Message::GradeListChangeCardAccept )
                .style( theme::Button::Positive )
            )
            .padding( 10 )
            .spacing( 20 )
            .align_items( Alignment::Center )
        )
        .width( iced::Length::Fixed( 400.0 ) )
        .height( iced::Length::Fixed( 400.0 ) )
        .into()
    }

    fn create_card_grade_delete( &self ) -> iced::Element<'_, Message>{
        Card::new(
            Text::new( "Noteneintrag löschen?" ),
            Text::new( format!( "Soll der Noteneintrag {} wirklich gelöscht werden?", self.grade ))
        ).foot(
            Row::new()
            .push(
                Button::new( "Abbrechen"  )
                .style( theme::Button::Secondary )
                .on_press( Message::GradeListChangeCardDeleteCancel )
                .padding( 10 )
            )
            .push( Space::with_width( iced::Length::Fill ))
            .push(
                Button::new( "Löschen" )
                .padding( 10 )
                .on_press( Message::GradeListChangeCardDeleteAccept )
                .style( theme::Button::Destructive )
            )
            .padding( 10 )
            .spacing( 20 )
            .align_items( Alignment::Center )
        )
        .width( iced::Length::Fixed( 400.0 ) )
        .height( iced::Length::Fixed( 400.0 ) )
        .into()
    }

    fn create_page_absences_list( &self ) -> iced::Element<'_, Message>{
        // Alle Elemente aus dem Vektor der Noten als Vektor
        let mut vec_element_absences: Vec<iced::Element<'_, Message>> = Vec::new();
        for absence in self.absences_list.clone(){
            let text_forename = Text::new( absence.forename.clone() ).width(100);
            let text_surname = Text::new( absence.surname.clone() ).width(100);
            let text_date = Text::new( absence.date()  ).width(100);
            let text_course = Text::new( absence.course.clone() ).width(150);
            let button = Button::new( icons::remove() ).style( theme::Button::Destructive ).on_press( Message::AbsencesListDeleteButtonClicked( absence.clone() ) );
            let row = Row::new()
                .push( text_forename )
                .push( text_surname )
                .push( text_date )
                .push( text_course )
                .push( button )
                .spacing( 20 )
                ;
            vec_element_absences.push( row.into() );
            vec_element_absences.push( Rule::horizontal( 10 ).into() );
        }
        let col_grades = Column::with_children( vec_element_absences )
            .spacing( 10 )
            .padding( 10 );
        // Scrollable Element, um alle Noten anzuzeigen
        let scrollable = Scrollable::new( col_grades );


        let header = Row::new()
            .push( Text::new( "Vorname" ).width(100) )
            .push( Text::new( "Nachname" ).width(100) )
            .push( Text::new( "Datum"  ).width(100) )
            .push( Text::new( "Kurs" ).width(150) )
            .spacing( 20 )
            .padding( 10 );

        Container::new(
            // Modal::new(
                // self.modal_state,
                Column::new()
                .push( self.section( "Liste der Abwesenheiten" ) )
                .push( header )
                .push( Rule::horizontal( 10 ) )
                .push( scrollable)
            //     ,
            //     Card::new(
            //         Text::new( "Abwesenheit löschen?" ),
            //         Text::new( format!( "Soll die Abwesenheit {} wirklich gelöscht werden?", self.absence ))
            //     ).foot(
            //         Row::new()
            //         .push(
            //             Button::new( "Abbrechen"  )
            //             .style( theme::Button::Secondary )
            //             .on_press( Message::AbsencesListDeleteCardCancel )
            //             .padding( 10 )
            //         )
            //         .push( Space::with_width( iced::Length::Fill ))
            //         .push(
            //             Button::new( "Löschen" )
            //             .padding( 10 )
            //             .on_press( Message::AbsencesListDeleteCardAccept( self.absence.id ) )
            //             .style( theme::Button::Destructive )
            //         )
            //         .padding( 10 )
            //         .spacing( 20 )
            //         .align_items( Alignment::Center )
            //     )
            //     .width( iced::Length::Fixed( 400.0 ) )
            //     .height( iced::Length::Fixed( 400.0 ) )
            //     .into()
            // )
            // .on_esc( Message::AbsencesListDeleteCardCancel )
            // .backdrop( Message::AbsencesListDeleteCardCancel )
        ).center_x().width(iced::Length::Fill).height(iced::Length::Fill)
        .into()
    }

    // Einstellungen
    fn create_page_settings( &self ) -> iced::Element<'_, Message>{
        let settings_theme = self.create_settings_row(
            "Theme",
            PickList::new(
                &CustomPalette::ALL[..],
                CustomPalette::from_string( &self.config.theme ),
                Message::SettingsThemeDarkMode
            ).into()
        );
        let settings_gradescale = self.create_settings_row(
            "Standard-Notenskala",
            PickList::new(
                &GradeScale::ALL[..],
                Some( self.config.gradescale ),
                Message::GradeScaleDefaultChanged
            ).into()
        );
        let scrollable = Scrollable::new(
            Column::new()
            .push( settings_theme )
            .push( settings_gradescale )
            .padding( 20 )
            .align_items( Alignment::Center )
        );
        Container::new( scrollable )
        .center_x().width(iced::Length::Fixed( 400.0 )).height(iced::Length::Fill).into()
    }

    fn create_settings_row( &self, text: &str, element: iced::Element<'static, Message> ) -> iced::Element<'_, Message>{
        Row::new()
        .push( Text::new( String::from( text ) ))
        .push( Space::new( iced::Length::Fill, iced::Length::Fixed( 10.0 )) )
        .push( element )
        .padding( 10 )
        .into()
    }

    // Erstellt Seite zur Auswahl des:der Schüler:in und der Vergabe der Note
    fn create_card_grade_taking( &self ) -> iced::Element<'_, Message>{
        let label_class_selected: iced::widget::text::Text = Text::new(format!(
            //"Gewählte Klasse: {}",
            "{}",
            self.course_selected
        )).size( 30 );

        // zufälliger Name
        let label_student_name: iced::widget::text::Text = Text::new(format!(
            //"Ausgewählt wurde: {}",
            "{}",
            self.student
        )).size( 25 );

        let button_skip = Tooltip::new(
            Button::new( icons::skip() )//"Schüler:in überspringen")
            .on_press( Message::Skip )
            .style( theme::Button::Secondary )
            .padding( 10 ),
            "Schüler:in überspringen",
            Position::Bottom,
        )
        .gap(10)
        .style(theme::Container::Box);
        let button_student_absend = Tooltip::new(
            Button::new( icons::absent() )//"Schüler:in abwesend")
            .on_press( Message::Absent )
            .style( theme::Button::Destructive )
            .padding( 10 ),
            "Schüler:in abwesend",
            Position::Bottom,
        )
        .gap(10)
        .style(theme::Container::Box);
        let row_pick_roll = Row::new()
            .push( label_student_name )
            .push(button_skip)
            .push(button_student_absend)
            .spacing( 20 )
            .align_items( Alignment::Center )
            ;

        // Baue alle Buttons für die Noten
        let row_grade_buttons = ForsiumMainWindow::create_button_grades( &self );

        let text_input_comment = TextInput::new(
            "Kommentar",
            &self.grade.comment.as_str(),
        )
        .on_input( Message::CommentChanged );

        let button_cancel = Tooltip::new(
            Button::new( icons::cancel() )//"Abbrechen" )
            .on_press( Message::CoursesAddWindowClose )
            .padding(10)
            .style( theme::Button::Destructive),
            "Abbrechen",
            Position::Bottom,
        )
        .gap(10)
        .style(theme::Container::Box);
        let button_set_grade = Tooltip::new(
            Button::new( icons::add_grade() )//"Note eintragen")
            .on_press(Message::SetGrade)
            .padding(10)
            .style( theme::Button::Positive),
            "Note eintragen",
            Position::Bottom,
        )
        .gap(10)
        .style(theme::Container::Box);
        let row_set_grade = Row::new()
            .push(button_cancel)
            .push( Space::with_width( iced::Length::Fill ) )
            .push(button_set_grade)
            .spacing(20)
            .align_items( Alignment::Center );
        let col_set_grade = Column::new()
            // .push(label_set_grade)
            .push(row_grade_buttons)
            .spacing(20)
            .align_items( Alignment::Center );

        Container::new(
            Card::new(
                Text::new( "Abfrage" ),
                Column::new()
                    .push(label_class_selected)
                    // .push(label_student_name)
                    .push(row_pick_roll)
                    .push(col_set_grade)
                    .push(text_input_comment)
                    .spacing(20)
                    .padding( 20 )
                    .align_items( Alignment::Center )
            )
            .foot(
                Row::new()
                .push(row_set_grade)
            )
            .width( iced::Length::Fixed( 600.0 ) )
            .height( iced::Length::Fixed( 400.0 ) )
        ).center_x().center_y().width(iced::Length::Fill).height(iced::Length::Fill).into()
    }

    fn create_button_grades( &self ) -> iced::Element<'_, Message>{
        // Erstellen einer Reihe an Buttons, die die Noten von 1-6 bzw. die Notenpunkte von 0-15 (Oberstufe) anzeigen
        // und diese jeweils in einer Message rausgeben.

        let mut vec_aussen: Vec<iced::Element<'_, Message>> = Vec::new();

        if !( self.student.grade_type == GradeType::Punkte ){
                let mut vec_buttons_integer: Vec<iced::Element<'_, Message>> = Vec::new();
                for i in vec!["1","2","3","4","5","6"]{
                    if i.parse::<f32>().unwrap() == self.grade.grade{
                        let button = Button::new( i )
                            .on_press(Message::GradeChanged( i.parse::<f32>().unwrap() ))
                            .padding( 10 )
                            .width( 50 )
                            .height( 40 );
                        vec_buttons_integer.push(button.into());
                    }
                    else {
                        let button = Button::new( i )
                            .on_press(Message::GradeChanged( i.parse::<f32>().unwrap() ))
                            .padding( 10 )
                            .width( 50 )
                            .height( 40 )
                            .style( theme::Button::Secondary );
                        vec_buttons_integer.push(button.into());
                    }
                }
                vec_aussen.push( Row::with_children( vec_buttons_integer )
                    .spacing( 5 ).into()
                );

            // Erstellen einer Reihe an Buttons, die die halbzahligen Noten 1-6 anzeigen
            // und diese jeweils in einer Message rausgeben.
            if self.gradescale == GradeScale::Halbzahlig || self.gradescale == GradeScale::Komplett{
                let mut vec_buttons_half: Vec<iced::Element<'_, Message>> = Vec::new();
                for i in vec!["1.5", "2.5", "3.5", "4.5", "5.5"]{
                    if i.parse::<f32>().unwrap() == self.grade.grade{
                        let button = Button::new( i )
                            .on_press(Message::GradeChanged( i.parse::<f32>().unwrap() ))
                            .padding( 10 )
                            .width( 50 )
                            .height( 40 );
                        vec_buttons_half.push(button.into());
                    }
                    else {
                        let button = Button::new( i )
                            .on_press(Message::GradeChanged( i.parse::<f32>().unwrap() ))
                            .padding( 10 )
                            .width( 50 )
                            .height( 40 )
                            .style( theme::Button::Secondary );
                        vec_buttons_half.push(button.into());
                    }
                }
                vec_aussen.push( Row::with_children( vec_buttons_half )
                    .spacing( 5 )
                    .into()
                );
            }
            // Erstellen einer Reihe an Buttons, die die drittelzahligen Noten 1-6 anzeigen
            // und diese jeweils in einer Message rausgeben.
            if self.gradescale == GradeScale::Drittelzahlig || self.gradescale == GradeScale::Komplett{
                let mut vec_buttons_half: Vec<iced::Element<'_, Message>> = Vec::new();
                for i in vec![ "1.3" , "1.7" , "2.3" , "2.7" , "3.3" , "3.7" , "4.3" , "4.7" , "5.3" , "5.7" ]{
                    if i.parse::<f32>().unwrap() == self.grade.grade{
                        let button = Button::new( i )
                            .on_press(Message::GradeChanged( i.parse::<f32>().unwrap() ))
                            .padding( 10 )
                            .width( 50 )
                            .height( 40 );
                        vec_buttons_half.push(button.into());
                    }
                    else {
                        let button = Button::new( i )
                            .on_press(Message::GradeChanged( i.parse::<f32>().unwrap() ))
                            .padding( 10 )
                            .width( 50 )
                            .height( 40 )
                            .style( theme::Button::Secondary );
                        vec_buttons_half.push(button.into());
                    }
                }
                vec_aussen.push( Row::with_children( vec_buttons_half )
                    .spacing( 5 )
                    .into()
                );
            }
        }
        // Erstellen einer Reihe an Buttons, die die Notenpunkte von 0-15 (Oberstufe) anzeigen
        // und diese jeweils in einer Message rausgeben.
        if self.student.grade_type == GradeType::Punkte {
            let mut vec_buttons_unter: Vec<iced::Element<'_, Message>> = Vec::new();
            for i in vec![ "0" , "1" , "2" , "3" , "4" ]{
                if i.parse::<f32>().unwrap() == self.grade.grade{
                    let button = Button::new( i )
                        .on_press(Message::GradeChanged( i.parse::<f32>().unwrap() ))
                        .padding( 10 )
                        .width( 50 )
                        .height( 40 );
                    vec_buttons_unter.push(button.into());
                }
                else {
                    let button = Button::new( i )
                        .on_press(Message::GradeChanged( i.parse::<f32>().unwrap() ))
                        .padding( 10 )
                        .width( 50 )
                        .height( 40 )
                        .style( theme::Button::Secondary );
                    vec_buttons_unter.push(button.into());
                }
            }
            vec_aussen.push( Row::with_children( vec_buttons_unter )
                .spacing( 5 )
                .into()
            );

            let mut vec_buttons_mitte: Vec<iced::Element<'_, Message>> = Vec::new();
            for i in vec![ "5" , "6" , "7" , "8" , "9" ]{
                if i.parse::<f32>().unwrap() == self.grade.grade{
                    let button = Button::new( i )
                        .on_press(Message::GradeChanged( i.parse::<f32>().unwrap() ))
                        .padding( 10 )
                        .width( 50 )
                        .height( 40 );
                    vec_buttons_mitte.push(button.into());
                }
                else {
                    let button = Button::new( i )
                        .on_press(Message::GradeChanged( i.parse::<f32>().unwrap() ))
                        .padding( 10 )
                        .width( 50 )
                        .height( 40 )
                        .style( theme::Button::Secondary );
                    vec_buttons_mitte.push(button.into());
                }
            }
            vec_aussen.push( Row::with_children( vec_buttons_mitte )
                .spacing( 5 )
                .into()
            );

            let mut vec_buttons_oben: Vec<iced::Element<'_, Message>> = Vec::new();
            for i in vec![ "10" , "11" , "12" , "13" , "14" , "15" ]{
                if i.parse::<f32>().unwrap() == self.grade.grade{
                    let button = Button::new( i )
                        .on_press(Message::GradeChanged( i.parse::<f32>().unwrap() ))
                        .padding( 10 )
                        .width( 50 )
                        .height( 40 );
                    vec_buttons_oben.push(button.into());
                }
                else {
                    let button = Button::new( i )
                        .on_press(Message::GradeChanged( i.parse::<f32>().unwrap() ))
                        .padding( 10 )
                        .width( 50 )
                        .height( 40 )
                        .style( theme::Button::Secondary );
                    vec_buttons_oben.push(button.into());
                }
            }
            vec_aussen.push( Row::with_children( vec_buttons_oben )
                .spacing( 5 )
                .into()
            );
        }

        // let col_aussen = Column::with_children( vec_aussen );
        // Erstellt aus den vorherigen Reihen eine Spalte an zentrierten Buttons.
        let col = Column::with_children( vec_aussen )
            .spacing(5)
            .align_items( Alignment::Center );

        col.into()
    }

    // Erstellt eine Spalte an Buttons, die allen in der Datenbank aufgelisteten
    // Kursen (Klasse + Fach) entspricht, und gibt diese als iced::Element zurück.
    fn create_button_courses( &self ) -> iced::Element<'_, Message>{
        let mut grid = Grid::with_column_width( 160.0 );
        let mut i=0;
        while i < self.courses.len() {
            let row = Row::new()
                .push( Button::new(
                        Text::new( self.courses[i].as_str() )
                        // .font( CHARISSIL )
                    )
                    .on_press(Message::CourseSelected( self.courses[i].clone() ))
                    .padding( 10 )
                    .height( 50 )
                    .width( 160 ) )
                .padding( 10 )
                .spacing( 10 )
                ;
            i = i+1;
            grid.insert( row );
        }
        Scrollable::new( grid )
        .height( iced::Length::Fixed(1000.0) ).into()
    }

    fn create_card_student_add( &self ) -> iced::Element<'_, Message> {
        Card::new(
            Text::new("Neuen Kurs hinzufügen"),
            Column::new()
                .push(TextInput::new(
                    "Vorname eintippen...",
                    self.student.forename.as_str()
                )
                .width( 200 )
                .on_input(Message::StudentInputChangedForename)
                .padding(10)
                )
                .push(TextInput::new(
                    "Nachname eintippen...",
                    self.student.surname.as_str()
                )
                .width( 200 )
                .on_input(Message::StudentInputChangedSurname)
                .padding(10)
                )
                .push(TextInput::new(
                    "Kurs eintippen...",
                    self.student.course.as_str()
                )
                .width( 200 )
                .on_input(Message::StudentInputChangedCourse)
                .padding(10)
                )
                .push(
                    Row::new()
                    .push(
                        Radio::new(
                            "Noten",
                            GradeType::Noten,
                            self.gradetype,
                            Message::GradeTypeChanged
                        )
                    )
                    .push(
                        Radio::new(
                            "Punkte",
                            GradeType::Punkte,
                            self.gradetype,
                            Message::GradeTypeChanged
                        )
                    )
                    .spacing( 20 )
                    .padding( 10 )
                )
                .spacing( 20 )
                .push(TextInput::new(
                    "Kommentar eintippen...",
                    self.grade.comment.as_str()
                )
                .width( 200 )
                .on_input(Message::StudentInputChangedComment)
                .padding(10)
                )
                .spacing( 20 )
                .align_items( Alignment::Center )
        )
        .foot( Row::new()
            .push(
                Button::new( "Abbrechen"  )
                .style( theme::Button::Destructive )
                .on_press( Message::CoursesAddWindowClose )
                .padding( 10 )
            )
            .push(
                self.button_tooltip(
                    String::from( "Aus Datei importieren" ),
                    icons::import(),
                    Message::DatabaseImportFromCSV,
                    theme::Button::Primary,
                    Position::Bottom
                )
                // Button::new( "Aus Datei importieren" )
                // .padding( 10 )
                // .on_press( Message::DatabaseImportFromCSV )
                // .style( theme::Button::Primary )
            )
            .padding( 10 )
            .spacing( 20 )
            .align_items( Alignment::Center )
            .push( Space::with_width( iced::Length::Fill ))
            .push(
                Button::new( "Hinzufügen" )
                .padding( 10 )
                .on_press( Message::CoursesAddWindowAccept )
                .style( theme::Button::Positive )
            )
            .padding( 10 )
            .spacing( 20 )
            .align_items( Alignment::Center )
        )
        .width( iced::Length::Fixed( 400.0 ) )
        .height( iced::Length::Fixed( 400.0 ) )
        .into()
    }

    fn create_sidebar( &self ) -> iced::Element<'_, Message>{
        match self.state {
            Layer::SelectDatabase => {
                let col = Column::new();
                Scrollable::new( col ).into()
            },
            _ => {
                let button_list_courses = self.button_tooltip(
                    String::from( "Kurse" ),
                    icons::courses_list(),
                    Message::StateChanged( Layer::SelectCourse ),
                    match self.state {
                        Layer::SelectCourse => theme::Button::Primary,
                        _ => theme::Button::Secondary,
                    },
                    Position::Right,
                );
                let button_list_grades = self.button_tooltip(
                    String::from( "Notenliste" ),
                    icons::grade_list(),
                    Message::StateChanged( Layer::ShowGrades ),
                    match self.state {
                        Layer::ShowGrades => theme::Button::Primary,
                        _ => theme::Button::Secondary,
                    },
                    Position::Right,
                );
                let button_list_absences = self.button_tooltip(
                    String::from( "Abwesenheiten" ),
                    icons::absences_list(),
                    Message::StateChanged( Layer::ShowAbsences ),
                    match self.state {
                        Layer::ShowAbsences => theme::Button::Primary,
                        _ => theme::Button::Secondary,
                    },
                    Position::Bottom,
                );
                let button_settings = self.button_tooltip(
                    String::from( "Einstellungen" ),
                    icons::settings(),
                    Message::StateChanged( Layer::Settings ),
                    match self.state {
                        Layer::Settings => theme::Button::Primary,
                        _ => theme::Button::Secondary,
                    },
                    Position::Right,
                );
                let col = Column::new()
                    .push( button_list_courses )
                    .push( button_list_grades )
                    .push( button_list_absences )
                    .push( button_settings )
                    .spacing( 10 )
                    .padding( iced::Padding::from( PADDING ) )
                ;
                Scrollable::new( col ).into()
            }
        }
    }

    fn button_tooltip( &self, text: String, icon: Text<'static>, message: Message, style: theme::Button, position: Position ) -> iced::Element<'_, Message>{
        Tooltip::new(
            Button::new( icon )
            .on_press( message )
            .padding( iced::Padding::from( PADDING ) )
            .style( style )
            ,
            text,
            position,
        )
        .style(theme::Container::Box)
        // .font( ANDIKA )
        .into()
    }

    fn section( &self, text: &str ) -> iced::Element<'_, Message>{
        Row::new()
        .push( Space::with_width( iced::Length::Fill ))
        .push(
            Text::new( String::from( text ) )
            .size( 30 )
        )
        .push( Space::with_width( iced::Length::Fill ))
        .align_items( Alignment::Center )
        .into()
    }
}
