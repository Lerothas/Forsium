use iced::layout::{self, Layout};
use iced::Renderer;
use iced::widget::{self, Widget,Button,Row,Column};
use iced::{Color, Element, Length, Point, Rectangle, Size, Alignment,Theme};


pub struct SelectGradeWidget<Message> {
    grade: f32,
    on_change: Box<dyn Fn(Option<f32>) -> Message>,
}

impl SelectGradeWidget {
    pub fn new(grade: f32) -> Self {
        Self { grade }
    }
}

 #[derive(Debug, Clone)]
 pub enum Event {
     GradeChanged
 }

pub fn SelectGradeWidget<Message>(
    grade: f32,
    on_change: impl Fn(Option<f32>) -> Message + 'static
) -> SelectGradeWidget<Message> {
    SelectGradeWidget::new(grade, on_change)
}

impl<Message, Renderer> Widget<Message, Renderer> for SelectGradeWidget
where
    Renderer: renderer::Renderer,

{
    fn width(&self) -> Length {
        Length::Shrink
    }

    fn height(&self) -> Length {
        Length::Shrink
    }

    fn layout(
        &self,
        _renderer: &Renderer,
        _limits: &layout::Limits,
    ) -> layout::Node {
        layout::Node::new(Size::new( 400.0, 400.0))
    }

    fn draw(
        &self,
        _state: &widget::Tree,
        renderer: &mut Renderer,
        _theme: &Renderer::Theme,
        _style: &renderer::Style,
        layout: Layout<'_>,
        _cursor_position: Point,
        _viewport: &Rectangle,
    ) //where <Renderer as iced_native::Renderer>::Theme: iced_native::widget::button::StyleSheet
    {
        // renderer.fill_quad(
        //     renderer::Quad {
        //         bounds: layout.bounds(),
        //         border_radius: self.radius.into(),
        //         border_width: 0.0,
        //         border_color: Color::TRANSPARENT,
        //     },
        //     Color::BLACK,
        // );
        let mut vec_buttons_integer: Vec<iced_native::Element<'_, Message, Renderer>> = Vec::new();
        for i in vec!["1","2","3","4","5","6"]{
            let button = Button::new( i )
                .on_press(Message::GradeChanged( i.parse::<f32>().unwrap() ))
                .padding( 10 )
                .width( iced_native::Length::FillPortion(50) )
                .height( iced_native::Length::FillPortion(40) );
            vec_buttons_integer.push(button.into());
        }
        let row_integer = Row::with_children( vec_buttons_integer )
            .spacing( 5 );

        // Erstellen einer Reihe an Buttons, die die halbzahligen Noten 1-6 anzeigen
        // und diese jeweils in einer Message rausgeben.
        let mut vec_buttons_half: Vec<iced_native::Element<'_, Message, Renderer>> = Vec::new();
        for i in vec!["1.5", "2.5", "3.5", "4.5", "5.5"]{
            let button = Button::new( i )
                .on_press(Message::GradeChanged( i.parse::<f32>().unwrap() ))
                .padding( 10 )
                .width( iced_native::Length::FillPortion(50) )
                .height( iced_native::Length::FillPortion(40) );
            vec_buttons_half.push(button.into());
        }
        let row_half = Row::with_children( vec_buttons_half )
            .spacing( 5 );

        // Erstellt aus den vorherigen Reihen eine Spalte an zentrierten Buttons.
        let col = Column::new()
            .push(row_integer)
            .push(row_half)
            .spacing(5)
            .align_items( Alignment::Center );

        col.into()
    }
}

impl<'a, Message, Renderer> From<SelectGradeWidget> for Element<'a, Message, Renderer>
where
    Renderer: renderer::Renderer,
{
    fn from(SelectGradeWidget: SelectGradeWidget) -> Self {
        Self::new(SelectGradeWidget)
    }
}
