// SPDX-FileCopyrightText: 2022 Stefan W. 'Lerothas' Draxlbauer <lerothas@mailbox.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use iced::Color;
use iced::theme::Theme;
use serde::Deserialize;
use serde::Serialize;

// #[allow(dead_code)]
#[derive(Debug,Deserialize,Serialize,Clone, Copy, PartialEq, Eq)]
pub enum CustomPalette{
    ArcDark,
    ArcLight,
    PopLight,
    LMLight,
    Light,
    Dark,
}
impl CustomPalette {
    pub const ALL: [CustomPalette; 6] = [
        CustomPalette::ArcDark,
        CustomPalette::ArcLight,
        CustomPalette::PopLight,
        CustomPalette::LMLight,
        CustomPalette::Light,
        CustomPalette::Dark,
    ];
}
impl Default for CustomPalette {
    fn default() -> CustomPalette{
        CustomPalette::Light
    }
}
impl std::fmt::Display for CustomPalette {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                CustomPalette::ArcDark => "Arc Dark",
                CustomPalette::ArcLight => "Arc Light",
                CustomPalette::PopLight => "Pop Light",
                CustomPalette::LMLight => "Linux Mint Light",
                CustomPalette::Light => "Light",
                CustomPalette::Dark => "Dark",
            }
        )
    }
}

#[allow(dead_code)]
impl CustomPalette{
    pub fn arc_light() -> iced::theme::Theme{
        iced::Theme::custom( PALETTE_ARC_LIGHT )
    }

    pub fn arc_dark() -> iced::theme::Theme{
        iced::Theme::custom( PALETTE_ARC_DARK )
    }

    pub fn pop() -> iced::theme::Theme{
        iced::Theme::custom( PALETTE_POP )
    }

    pub fn light() -> iced::theme::Theme{
        Theme::Light
    }

    pub fn dark() -> iced::theme::Theme{
        Theme::Dark
    }

    pub fn from_string( string: &String ) -> Option<CustomPalette>{
        if string.eq( "Arc Dark" ){
            Some(CustomPalette::ArcDark)
        }
        else if string.eq( "Arc Light" ){
            Some(CustomPalette::ArcLight)
        }
        else if string.eq( "Pop Light" ){
            Some(CustomPalette::PopLight)
        }
        else if string.eq( "Linux Mint Light" ){
            Some(CustomPalette::LMLight )
        }
        else if string.eq( "Light" ){
            Some(CustomPalette::Light)
        }
        else if string.eq( "Dark" ){
            Some(CustomPalette::Dark)
        }
        else{
            Some(CustomPalette::Light)
        }
    }

    pub fn theme_from_string( string: &String ) -> iced::theme::Theme{
        if string.eq( "Arc Dark" ){
            iced::Theme::custom( PALETTE_ARC_DARK )
        }
        else if string.eq( "Arc Light" ){
            iced::Theme::custom( PALETTE_ARC_LIGHT )
        }
        else if string.eq( "Pop Light" ){
            iced::Theme::custom( PALETTE_POP )
        }
        else if string.eq( "Linux Mint Light" ){
            iced::Theme::custom( PALETTE_LM_LIGHT )
        }
        else if string.eq( "Light" ){
            Theme::Light
        }
        else if string.eq( "Dark" ){
            Theme::Dark
        }
        else{
            Theme::Light
        }
    }

    pub fn theme( palette: &CustomPalette ) -> iced::theme::Theme{
        match palette{
            CustomPalette::ArcDark => iced::Theme::custom( PALETTE_ARC_DARK ),
            CustomPalette::ArcLight => iced::Theme::custom( PALETTE_ARC_LIGHT ),
            CustomPalette::PopLight => iced::Theme::custom( PALETTE_POP ),
            CustomPalette::LMLight => iced::Theme::custom( PALETTE_LM_LIGHT ),
            CustomPalette::Light => Theme::Light,
            CustomPalette::Dark => Theme::Dark,
        }
    }
}

#[allow(dead_code)]
pub const PALETTE_ARC_DARK: iced::theme::Palette = iced::theme::Palette{
    // Arc-Theme:
    background: Color::from_rgb( 0.25, 0.27, 0.32 ),
    text: Color::from_rgb( 0.77, 0.80, 0.83 ),
    primary: Color::from_rgb( 0.32, 0.58, 0.89 ),
    success: Color::from_rgb( 0.06, 0.71, 0.69 ),
    danger: Color::from_rgb( 0.94, 0.29, 0.31 ),
};

#[allow(dead_code)]
pub const PALETTE_ARC_LIGHT: iced::theme::Palette = iced::theme::Palette{
    // Arc-Theme:
    background: Color::from_rgb( 1.0, 1.0, 1.0 ),
    text: Color::from_rgb( 0.77, 0.80, 0.83 ),
    primary: Color::from_rgb( 0.32, 0.58, 0.89 ),
    success: Color::from_rgb( 0.06, 0.71, 0.69 ),
    danger: Color::from_rgb( 0.94, 0.29, 0.31 ),
};

#[allow(dead_code)]
pub const PALETTE_POP: iced::theme::Palette = iced::theme::Palette{
    // Pop-Theme
    background: Color::from_rgb(0.98, 0.98, 0.98),
    text: Color::BLACK,
    primary: Color::from_rgb(0.18, 0.24, 0.31),
    success: Color::from_rgb(0.51, 0.75, 0.55),
    danger: Color::from_rgb(0.90, 0.42, 0.33),
};

#[allow(dead_code)]
pub const PALETTE_LM_LIGHT: iced::theme::Palette = iced::theme::Palette{
    background: Color::from_rgb(0.91, 0.91, 0.91),
    text: Color::BLACK,
    primary: Color::from_rgb(0.05, 0.46, 0.87),
    success: Color::from_rgb(0.52, 0.75, 0.39),
    danger: Color::from_rgb(0.91, 0.13, 0.15),
};
