// SPDX-FileCopyrightText: 2022 Stefan W. 'Lerothas' Draxlbauer <lerothas@mailbox.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::gradescale::GradeType as GradeType;

// Konstrukt für Schüler
#[derive(Clone,Debug)]
pub struct Student{
    pub id: u32,
    pub surname: String,
    pub forename: String,
    pub counter: u32,
    pub course: String,
    pub grade_type: GradeType,
    pub comment: String,
    pub place_number: u32,
}
impl Default for Student {
    fn default() -> Student{
        Student{
            id: 0,
            forename: String::from( "" ),
            surname: String::from( "" ),
            counter: 2,
            course: String::from( "" ),
            grade_type: GradeType::Noten,
            comment: String::from( "" ),
            place_number: 0,
        }
    }
}

#[allow(dead_code)]
impl Student{
    pub fn name( &self ) -> String{
        let vor = &self.forename;
        let nach = &self.surname;
        format!( "{vor} {nach}" )//, self.vorname, self.nachname )
    }
}
impl std::fmt::Display for Student {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!( f, "{} {}", &self.forename, &self.surname )
    }
}
